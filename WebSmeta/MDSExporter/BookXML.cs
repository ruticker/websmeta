﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
namespace MDSExporter
{
    public class Book
    {
        public int TPBASE { get; set; }
        public int TPBOOK { get; set; }
        public string OBOSNOV { get; set; }
        public string NAME { get; set; }
        public string COD { get; set; }
        public string SOKROBOZ { get; set; }
        public int DEFINITION { get; set; }
        public List<TitleTree> CHAPTER { get; set; }
    }
    public class TitleTree
    {
        public TitleTree(XElement xmlTree)
        {
            NAME=xmlTree.Attribute("NR").Value;
            if (xmlTree.Attribute("OB")!=null)
                COD=xmlTree.Attribute("OB").Value;
            if (xmlTree.Elements().Any())
            {
                CHAPTER=new List<TitleTree>();
                foreach (var node in xmlTree.Elements())
                {
                    CHAPTER.Add(new TitleTree(node));
                }
            }
        }
        public TitleTree(string xml) : this(XElement.Parse(xml))
        {
        }
        public TitleTree()
        {
        }
        public string NAME { get; set; }
        public string COD { get; set; }
        public List<TitleTree> CHAPTER { get; set; }
    }
}
