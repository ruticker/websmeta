﻿using BinSacnner;
using FirebirdSql.Data.FirebirdClient;
using Newtonsoft.Json;
using Org.BouncyCastle.Utilities.Zlib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace MDSExporter
{
    public class DbReader
    {
        protected bool SaveData(string FileName, byte[] Data)
        {
            BinaryWriter Writer = null;
            string Name = FileName;// @"C:\temp\yourfile.name";
            try
            {
                // Create a new stream to write to the file
                Writer = new BinaryWriter(File.OpenWrite(Name));
                // Writer raw data                
                Writer.Write(Data);
                Writer.Flush();
                Writer.Close();
            }
            catch
            {
                //...
                return false;
            }
            return true;
        }
        protected byte[] LoadData(string FileName)
        {
            var Reader = new BinaryReader(File.Open(FileName, FileMode.Open));
            return Reader.ReadAllBytes();
            //Blowfish decoder = new Blowfish(Encoding.ASCII.GetBytes(Blowfish.key));
            //var s=  decoder.DecodeBin(res);
        }
        private FbConnection GetConnection(string db)
        {
            string conn = "User=SYSDBA;" +
                            "Password=masterkey;" +
                            "Database=" + db + ";" +
                            "DataSource=localhost;" +
                            "Port=3050;" +
                            "Dialect=3;" +
                            "Charset=win1251;" +
                            "Role=;" +
                            "Connection lifetime=15;" +
                            "Pooling=true;" +
                            "MinPoolSize=0;" +
                            "MaxPoolSize=50;" +
                            "Packet Size=8192;" +
                            "ServerType=0";
            return new FbConnection(conn);
        }
        private IEnumerable<string> AllTables(string db)
        {
            var myConnection = GetConnection(db);
            string sql = "select rdb$relation_name from rdb$relations where rdb$view_blr is null and(rdb$system_flag is null or rdb$system_flag = 0);";
            FbCommand myCommand = new FbCommand(sql, myConnection);
            myConnection.Open();
            myCommand.CommandTimeout = 0;
            FbDataReader myReader = myCommand.ExecuteReader();
            while (myReader.Read())
                yield return (myReader[0] as string).Trim();
            myConnection.Close();
        }
        ///"0000ГЕ29000000040000190003000000000000"
        ///  if (s2 == "ГЕ29-4-19-3")
        string trim(string s)
        {
            while (s.Length > 0 && s[0] == '0')
                s = s.Substring(1);
            return s;
        }
        string Obosnov(string s)
        {
            //0000ГЕ60
            try
            {
                string name = trim(s.Substring(0, 8));
                string s1 = trim(s.Substring(8, 8));
                string s2 = trim(s.Substring(16, 6));
                string s3 = trim(s.Substring(22, 4));
                string s4 = trim(s.Substring(26, 4));
                if (s1.Length > 0)
                    name += "-" + s1;
                if (s2.Length > 0)
                    name += "-" + s2;
                if (s3.Length > 0)
                    name += "-" + s3;
                if (s4.Length > 0)
                    name += "-" + s4;
                return name.Substring(0, Math.Min(name.Length, 16));
            }
            catch
            {
                return s;
            }
        }
        Blowfish LispravkiDecoder = new Blowfish(Encoding.GetEncoding(1251).GetBytes(Blowfish.LisrpopravkiKey));
        Blowfish P0Decoder = new Blowfish(Encoding.GetEncoding(1251).GetBytes(Blowfish.P0Key));
        public void PostProcessListpopravki(Dictionary<String, object> fields)
        {
            string koef = LispravkiDecoder.DecodeMDS(fields["KOEF"] as string);
            fields["NAME"] = LispravkiDecoder.DecodeMDS(fields["NAME"] as string);
            fields["NAMEP"] = LispravkiDecoder.DecodeMDS(fields["NAMEP"] as string);
            fields["NUMBER"] = (fields["NUMBER"] as string).Trim();
            fields["POPR"] = (fields["POPR"] as string).Trim();
            decimal? Labor = null;
            decimal? Machine = null;
            decimal? Material = null;
            if (koef.IndexOf('$') != -1)
                koef = koef.Substring(0, koef.IndexOf('$'));
            foreach (var s in koef.Split(';'))
            {
                var parts = s.Split('=');
                if (parts.Length == 2)
                {
                    int idx = -1;
                    int.TryParse(parts[0], out idx);
                    decimal val = decimal.Parse((parts[1]).Replace("$", ""), CultureInfo.InvariantCulture);
                    switch (idx)
                    {
                        case 2:
                            Labor = val;
                            break;
                        case 7:
                            Machine = val;
                            break;
                        case 8:
                            Material = val;
                            break;
                    }
                }
            }
            fields["Labor"] = Labor;
            fields["Machine"] = Machine;
            fields["Material"] = Material;
            fields.Remove("KOEF");
        }
        public void PostProcessPoprav(Dictionary<String, object> fields)
        {
            fields["OBOSNOV"] = (fields["OBOSNOV"] as string).Trim();
            string lpopr = LispravkiDecoder.DecodeMDS(fields["LPOPR"] as string);
            string prefix = (fields["OBOSNOV"] as string);
            prefix = prefix.Substring(0, prefix.IndexOf("-") + 1);
            if (lpopr != "")
                fields["LPOPR"] = lpopr.Trim(';').Split(';').Select(x => prefix + x).ToArray();
            else
                fields["LPOPR"] = new string[0];
        }
        private double? ParseDoubleNull(string s)
        {
            if (s == null)
                return null;
            double d;
            if (double.TryParse(s, NumberStyles.Number, CultureInfo.GetCultureInfo("ru-RU"), out d))
                return d;
            return null;
        }
        public void PostProcessP0(Dictionary<String, object> fields)
        {
            fields["OBOSNOV"] = (fields["OBOSNOV"] as string).Trim();
            string rprice = fields["RPRICE"] as string;
            if (rprice == null)
                return;
            if (P0Decoder.CheckEncoded(rprice))
            {
                string aa = P0Decoder.DecodeMDS(rprice);
                if (aa != null)
                    rprice = aa;
            }
            string aaa = rprice;
            double?[] values;
            try
            {
                //   rprice=rprice.Replace("\u0001", "#").Replace("\u001c", "");
                string[] ps = rprice.Split('#');
                int id = int.Parse(ps[0]);
                // if (int.TryParse(ps[0], out id))
                values = ps.Skip(1).Select(x => ParseDoubleNull(x)).ToArray();
                ///else
                   // values=ps.Select(x => ParseDoubleNull(x)).ToArray();
                fields.Remove("RPRICE");
                switch (id)
                {
                    case 1:
                        fields["Task"] = new
                        {
                            Total = values[0],
                            LaborCost = values[1],
                            Machine = values[2],
                            Salary = values[3],
                            Materials = values[4],
                            Labor = values[6],
                            Machinist = values[7]
                        };
                        break;
                    case 2:
                        fields["Material"] = new
                        {
                            Smetnaya = values[0],
                            Otpusk = values[1]
                        };
                        break;
                    case 4:
                        fields["Transport"] = new
                        {
                            Price = values[1]
                        };
                        break;
                    default:
                        throw new SystemException("ssss");
                }
            }
            catch (Exception e)
            {
            }
        }
        public void PostProcessVidrabot(Dictionary<String, object> fields)
        {
            fields["OBOSNOV"] = Obosnov(fields["OBOSNOV"] as string);
        }

        public Dictionary<int,int> ax= new Dictionary<int,int>();

        public void PostProcessS0(Dictionary<String, object> fields)
        {
            fields["OBOSNOV"] = Obosnov(fields["OBOSNOV"] as string);
            byte[] arr = fields["RS"] as byte[];
            if (arr != null && arr.Length > 0)
            {
                Cast5Engine c = new Cast5Engine();
                c.Init(false, Encoding.GetEncoding(1251).GetBytes(fields["OBOSNOV"] as string).Take(16).ToArray());
                c.Decrypt(arr);
                var rr = new Reader(arr).ReadStream<Sclass>();

                ax[rr.Type] = 0;
                /*
                if (rr.Type!=0  && rr.Type!=65 && rr.Type != 1 
                    && rr.Type != 2 && rr.Type != 3 && rr.Type != 66 && rr.Type != 4 && rr.Type != 5
                    && rr.Type != 6 && rr.Type != 7 && rr.Type != 8 && rr.Type != 67 && rr.Type != 9
                    && rr.Type != 71 && rr.Type != 10 && rr.Type != 11 && rr.Type != 12 && rr.Type != 15
                    && rr.Type != 18 && rr.Type != 56 && rr.Type != 59 && rr.Type != 62 && rr.Type != 29
                    && rr.Type != 19 && rr.Type != 20 && rr.Type != 21 && rr.Type != 22 && rr.Type != 64
                    && rr.Type != 63 && rr.Type != 23
                    )
                    rr = rr;*/
                fields["RS"] = rr;
            }
        }
        public void PostProcessDocuments(Dictionary<String, object> fields)
        {
            byte[] arr = fields["LIST"] as byte[];
            if (arr != null && arr.Length > 0)
            {
                var o = new List<byte>();
                using (ZInputStream s = new ZInputStream(new MemoryStream(arr)))
                {
                    int bx;
                    while ((bx = s.ReadByte()) > -1)
                        o.Add((byte)bx);
                    fields["LIST"] = o.ToArray();
                }
            }
            fields["COMMONO"] = new Reader(fields["COMMONO"] as byte[]).ReadStream<Commono>();
        }
        public void PostProcessBook(Dictionary<String, object> fields)
        {
            if (!String.IsNullOrWhiteSpace(fields["CHAPTER"] as string))
                fields["CHAPTER"] = new TitleTree(fields["CHAPTER"] as string);
        }
        int cnt = 0;
        public void PostProcessTable(string tableName, Dictionary<String, object> fields)
        {
            for (int i = 0; i < fields.Keys.Count; i++)
            {
                var k = fields.Keys.ElementAt(i);
                if (fields[k] is string && !string.IsNullOrWhiteSpace(fields[k] as string) && !P0Decoder.CheckEncoded(fields[k] as string))
                {
                    fields[k] = (fields[k] as string).Trim();
                }
            }
            if (tableName == "BOOK")
                PostProcessBook(fields);
            else
            if (tableName == "VIDRABOT")
                PostProcessVidrabot(fields);
            else
            if (tableName == "POPRAV")
                PostProcessPoprav(fields);
            else
            if (tableName == "LISTPOPRAVKI")
                PostProcessListpopravki(fields);
            else
            if (tableName.StartsWith("P0"))
                PostProcessP0(fields);
            else
            if (tableName.StartsWith("S0"))
                PostProcessS0(fields);
            else
                if (tableName == "DOCUMENTS")
                PostProcessDocuments(fields);
            for (int i = 0; i < fields.Keys.Count; i++)
            {
                var k = fields.Keys.ElementAt(i);
                if (fields[k] is byte[] && (fields[k] as byte[]).Length > 0)
                {
                    fields[k] = saveBlob(tableName, k + "_" + cnt++, fields[k] as byte[]).Trim();
                }
            }
        }
        private IEnumerable<object> ExecuteSQL(string db, string tableName)
        {
            string sql = "SELECT * FROM  " + tableName;
            var myConnection = GetConnection(db);
            FbCommand myCommand = new FbCommand(sql, myConnection);
            myConnection.Open();
            myCommand.CommandTimeout = 0;
            FbDataReader myReader = myCommand.ExecuteReader();
            var excluded = new HashSet<int>();
            while (myReader.Read())
            {
                Dictionary<String, object> fields = new Dictionary<string, object>();
                for (int i = 0; i < myReader.FieldCount; i++)
                    fields[myReader.GetName(i)] = myReader[i];
                PostProcessTable(tableName, fields);
                yield return fields;
            }
            myConnection.Close();
        }
        public IEnumerable<string> ProcessDirectory(string targetDirectory, string mask = "*.g*")
        {
            foreach (var f in Directory.GetFiles(targetDirectory, mask))
                yield return f;
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
                foreach (var f in ProcessDirectory(subdirectory))
                    yield return f;
        }
       
        public void decodeReports()
        {
            IdeaEngine c = new IdeaEngine();
            c.Init(false, Encoding.GetEncoding(1251).GetBytes("Zg&wnYT^@Opl93H#Lq"));
            var files = ProcessDirectory("C:/CLG/ru/", "*.rep").ToArray();
            foreach (var f in files)
            {
                var b = LoadData(f);
                c.Decrypt(b);
                var s = f.Replace(".rep", ".fr3");
                File.WriteAllBytes(s, b);
            }
        }
        void CreateIfExsists(string subPath)
        {
            if (!System.IO.Directory.Exists(subPath))
                System.IO.Directory.CreateDirectory(subPath);
        }
        string saveBlob(string table, string id, byte[] blob)
        {
            string path = currPath + "files/";
            CreateIfExsists(path);
            path += table + "/";
            CreateIfExsists(path);
            string f = path + id;
            File.WriteAllBytes(f, blob);
            return f;
        }
        string currPath = "";
        public void Process()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            // string[] Bases = { "C:/CLG/CMETA2016_trial/RU/Data/101017.GDn" };
            //  string[] Bases = { "C:/CLG/CMETA2016_trial/RU/Data/project.GDp" };
            var  Bases=ProcessDirectory("C:/dbs/", "2*.g*").ToArray();
          //  var Bases = ProcessDirectory("C:/CLG/CMETA2016_trial/RU/Data/").ToArray();
            foreach (var s in Bases)
                Console.WriteLine(s);
            foreach (var Base in Bases)
            {
                var db = Path.GetFileNameWithoutExtension(Base);
                string subPath = "c:/db1/" + db;
                currPath = subPath + "/";
                CreateIfExsists(subPath);
                foreach (string tab in AllTables(Base))
                {
                 //   if (tab.Contains("S0"))
                    using (TextWriter streamWriter = new StreamWriter(subPath + "/" + tab))
                    {
                        var table = ExecuteSQL(Base, tab).ToList();
                        string s = // JsonConvert.SerializeObject(table);

                        JsonConvert.SerializeObject(table, Formatting.Indented, new DecimalJsonConverter());

                        streamWriter.WriteLine(s);
                    }
                }
            }
        }
    }
}
