﻿using System;
using Newtonsoft.Json;
namespace BinSacnner
{
    //"Stree1": [ "19-00-00-00-15-00-01-00-00-00-03-00-00-00-02", "Бульдозеры, мощность 79 кВт (108 л.с.)", "9101-1-35", "00-00", "маш.-ч", "00-00-00-00 -00-00-00-00- 00-00-00-00-00-00", "-1.0*(2,5 * 10,27)", "CC-CC-CC-CC- CC-AC-39-C0-   00-00-00-00-00-00-00-00 -00-00-00-00-00-00-00-00  -00-00-00-00- CC-CC-CC-CC-CC-AC-39-C0-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00", 79.07, 79.07, 13.5, 1, 4, "88-B7-32-01-00-00", " (исключается стоимость ресурса)", "00" ],
    public class Baser
    {

    }


    public class stree1class
    {
        public int i1 { get; set; }
        public short i2 { get; set; }
        public int Type { get; set; }
        public int LineNumber { get; set; }
        public byte i5 { get; set; }

        public string Name { get; set; }
        public string Cod { get; set; }
        public string unkZeroSTR { get; set; }
        public string rsize { get; set; }
        public string Works { get; set; }

        [Size(3)]
        public int[] UnknownInts { get; set; }

        public string mnoj { get; set; }

        public double Obem1 { get; set; }
        [Size(20)]
        public object udata1 { get; set; }
        public double Obem2 { get; set; }

        [Size(24)]
        public object udata2 { get; set; }

        public double SmetStoim { get; set; }
        public double OtpuskCena { get; set; }
        public double vTchZp { get; set; }
        public double TranspZatr { get; set; }
        public double ZSrashod { get; set; }


        public int DBCODE { get; set; }
        public short i7 { get; set; }

        public string comment { get; set; }

        public byte b1 { get; set; }

        public object qlast { get; set; }
    }
    public class ArrayWrapper<T>
    {
        public T[] value { get; set; }
    }

    public class Header
    {
        public int num1 { get; set; }
        public int num2 { get; set; }
        public int num3 { get; set; }
        public int num4 { get; set; }
        public string paragraph { get; set; }
        public string name { get; set; }
        public short someZero { get; set; }
    }
    [Chunker]
    public class BlockContainer
    {
        public byte[] Part1 { get; set; }
        public byte[] Part2 { get; set; }
        public byte[] Part3 { get; set; }
    }

    [Chunker]
    public class IntContainer
    {
        public int val { get; set; }
    }

    [Chunker]
    public class ChuncksZBody : Baser
    {
        public body Part1 { get; set; }
        public byte Part2 { get; set; }
        public int Part3 { get; set; }
        public BlockContainer Part4 { get; set; }
        public int Part5 { get; set; }
        public IntContainer Ints { get; set; }
        public Guid qguid { get; set; }
    }

    [Chunker]
    public class Razdelx : Baser
    {
        public stree1class s1s1 { get; set; }
        public RES RES { get; set; }
        public MagicBlock MagicBlock { get; set; }
        public object Stree4 { get; set; }
        public ArrayWrapper<double> Stree5 { get; set; }
        public byte S1 { get; set; }
        public int zeroInt_1 { get; set; }
        public int zeroInt_2 { get; set; }
        public byte S4 { get; set; }
        public s5class S5 { get; set; }
        public int zeroInt_3 { get; set; }
        public int zeroInt_4 { get; set; }
        public byte S8 { get; set; }
        public s9class S9 { get; set; }
        public Guid? GuidNull { get; set; }
        public BlockContainer S12 { get; set; }
        public IntContainer S13 { get; set; }
        public short zeroShort_1 { get; set; }
        public DbVersion DbVersion { get; set; }
        public string LawDateStr { get; set; }
        public object S17 { get; set; }
        public Guid Guid1 { get; set; }
        public object S19 { get; set; }
        public object S20 { get; set; }
        public object S21 { get; set; }
    }


    public class ElevenBlock
    {
        public double param1 { get; set; }
        public short param2 { get; set; }
        public byte param3 { get; set; }
    }
    public class SeventeenBlock
    {
        public double param1 { get; set; }
        public double param2 { get; set; }
        public byte param3 { get; set; }
    }
    public class Block13
    {
        public double param1 { get; set; }
        public int param2 { get; set; }
        public byte param3 { get; set; }
    }

    public class SmetaBlock
    {
        public int ZeroIntDelimeter { get; set; }

        [DynamicClass("GetFType")]
        public Baser Body { get; set; }

        public Type GetFType()
        {
            if (ZeroIntDelimeter == 100)
                return typeof(Razdelx);

            return typeof(ChuncksZBody);
        }
    }

    public class body
    {
        public Header Header { get; set; }
        public MagicBlock magicBlock { get; set; }

        public int size { get; set; }
        [SizeFormProperty("size")]
        public SmetaBlock[] Razdels { get; set; }
        public object zzlast { get; set; }
    }


    


    [Chunker]
    public class s9class
    {
        public class O1
        {
            [Size(5)]
            public double[] doubles { get; set; }
            public string formula { get; set; }
        }

        public O1 o1 { get; set; }
        public ArrayWrapper<double> o2 { get; set; }
        public double doubleo3 { get; set; }
        public double doubleo4 { get; set; }
        public ArrayWrapper<byte> o5 { get; set; }
    }

    public class Federal
    {
        public int DBCODE { get; set; }
        public string Redaction { get; set; }
        public string Version { get; set; }
        public int index { get; set; }
    }

    [Chunker]
    public class DbVersion
    {
        public Federal Federal { get; set; }
        public string Comment { get; set; }
    }
    public class s34wp
    {
        public int i1 { get; set; }
        public int i2 { get; set; }
        public int ix3 { get; set; }
    }

    [Chunker]
    public class s5class
    {
        public ArrayWrapper<double> o1 { get; set; }
        public ArrayWrapper<double> o2 { get; set; }
    }
    [Chunker]
    public class s5class1
    {
        public ArrayWrapper<double> o1 { get; set; }
        public ArrayWrapper<double> o2 { get; set; }
    }
    [Chunker]
    public class MagicBlock
    {
        public ArrayWrapper<ElevenBlock> EW1 { get; set; }
        public int i1 { get; set; }
        public ArrayWrapper<ElevenBlock> EW2 { get; set; }
        public ArrayWrapper<ElevenBlock> EW3 { get; set; }
        public ArrayWrapper<ElevenBlock> EW4 { get; set; }
        public ArrayWrapper<ElevenBlock> EW5 { get; set; }
        public ArrayWrapper<ElevenBlock> EW6 { get; set; }
        public byte[] Data1 { get; set; }
        public ArrayWrapper<Block13> W13_1 { get; set; }
        public ArrayWrapper<Block13> W13_2 { get; set; }
        public ArrayWrapper<Block13> W13_3 { get; set; }
        public ArrayWrapper<Block13> W13_4 { get; set; }
        public object Unk1 { get; set; }
        public int Unk2 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW11 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW12 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW13 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW14 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW15 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW16 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW17 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW18 { get; set; }
        public object Last { get; set; }
    }
    public class RES
    {
        public int zcount { get; set; }
        [SizeFormProperty("zcount")]
        public Res[] fields { get; set; }
    }
    public class BuidRespource
    {
        public string COD { get; set; }
        public string Name { get; set; }
        public string Rsize { get; set; }
        [Size(10)]
        public double[] values { get; set; }
        public byte b1 { get; set; }
        public byte b2 { get; set; }
        public int DBCODE { get; set; }
        public short sx1 { get; set; }
        public double d1 { get; set; }
        public double d2 { get; set; }
        public double d3 { get; set; }
        public object last { get; set; }

        /*
        public int i1 { get; set; }
        public int i2 { get; set; }*/
    }

    public class Q2
    {
        public object qdata { get; set; }
    }


    [Chunker]
    public class QQ
    {
        public class secondField
        {
            public byte b1 { get; set; }
            public double d1 { get; set; }
        }


        public class o2Field
        {
            public int int_val { get; set; }
            [Size(4)]
            public double[] doubles { get; set; }
        }

        [Size(5)]
        public string[] StringNumg { get; set; }
        public secondField second_Field { get; set; }

        public object o1 { get; set; }
        public o2Field o2 { get; set; }
        public object o3 { get; set; }
        public object o4 { get; set; }
        public object o5 { get; set; }
        public object o6 { get; set; }
    }




    public class Q2S
    {
        public int sign { get; set; }

        public class Q2Sx1
        {
            public int ZERO { get; set; }
            public QQ qdata { get; set; }
        }

        public class Q2Sx2 
        {
            public object qlist { get; set; }
           // public int ZERO { get; set; }
        }

        [DynamicClass("GetFType")]
        public object Body { get; set; }

        public Type GetFType()
        {
            if (sign == 0)
                return typeof(Q2Sx1);

            return typeof(Q2Sx2);
        }
    }

    public class Q5
    {
        public byte num { get; set; }
        public string code { get; set; }
    }

    [Chunker]
    public class Res
    {
        public BuidRespource BuidRespource { get; set; }
        public Q2S qxx { get; set; }
        public double doubleq3 { get; set; }
        public int q4 { get; set; }
        public Q5 q5 { get; set; }
        public s5class1 q6 { get; set; }
        public short q7 { get; set; }
        public Guid? guid1 { get; set; }
        public string Redaction { get; set; }
        public DbVersion DbVersion { get; set; }
        public int q11 { get; set; }
        public Guid guid2 { get; set; }
        public int q13 { get; set; }
        public string Primechanie { get; set; }
        public byte q15 { get; set; }
        public short q16 { get; set; }
    }



    [Chunker]
    public class Lawclass
    {
        public string Info { get; set; }
    }
    public class Subwork
    {
        public string OBOSNOV { get; set; }
        public byte Type { get; set; }
        public double Value { get; set; }
    }
    public class Subwork2
    {
        public string OBOSNOV { get; set; }
        public byte Type { get; set; }
        public double Value { get; set; }
        public string Name { get; set; }
        public string Rsize { get; set; }
    }

    public class Commono
    {
        public byte BYTE { get; set; }
        public string NAME { get; set; }
        public Guid GUID { get; set; }
    }

    public class Sclass
    {
        public int Sign { get; set; }
        public string Name { get; set; }
        public string Rsize { get; set; }
        public string Info { get; set; }
        public int Type { get; set; }
        public double DoubleCoef { get; set; }
        public string Works { get; set; }
        [JsonIgnore]
        public int ZERO { get; set; }
        [JsonIgnore]
        public int ONE { get; set; }
        public Lawclass law { get; set; }

        [JsonIgnore]
        public int Count { get; set; }

        

        [SizeFormProperty("Count")]
        public Subwork[] Subworks { get; set; }

        [JsonIgnore]
        public byte some { get; set; }
        /*
        public short some1 { get; set; }

        [JsonIgnore]
        public int count2 { get; set; }


        [SizeFormProperty("count2")]
        public Subwork2[] Subworks2 { get; set; }

        [JsonIgnore]
        public byte[] last { get; set; } */
    }

}

