﻿namespace BinSacnner
{

    public class DynamicClassAttribute : System.Attribute
    {
        public string Name { get; set; }
        public DynamicClassAttribute()
        {
        }
        public DynamicClassAttribute(string function)
        {
            Name = function;
        }
    }

    public class SizeAttribute : System.Attribute
    {
        public int Size { get; set; }
        public SizeAttribute()
        {
        }
        public SizeAttribute(int size)
        {
            Size = size;
        }
    }
    public class SizeFormPropertyAttribute : System.Attribute
    {
        public string Name { get; set; }
        public SizeFormPropertyAttribute()
        {
        }
        public SizeFormPropertyAttribute(string name)
        {
            Name = name;
        }
    }
    public class ChunkerAttribute : System.Attribute
    {     
        public ChunkerAttribute()
        {
        }
    }
}
/*
 * 
 * 
 *  public static object ByteReccurent(byte[] arr)
        {
            var reader = new MegaReader(arr);
            short id1 = 0;
            while (id1 == 0)
            {
                if (!reader.CanRead<short>())
                    goto fail;
                reader.SavePosition();
                id1 = reader.ReadStream<short>();
            }
            if (id1 != 1)
                goto fail;
            reader.RollBack();
            List<object> l = new List<object>();
            while (true)
            {
                if (reader.CanRead<byte>(6))
                {
                    short id = reader.ReadStream<short>();
                    if (id == -1)
                    {
                        int sign = reader.ReadStream<int>();
                        if (sign == 3)
                        {
                            reader.ReadStream<int>();
                            var data = reader.ReadArray<byte>(reader.arr.Length - reader.currptr);
                            l.Add(new
                            {
                                id,
                                sign,
                                size = reader.arr.Length - reader.currptr,
                                data = ByteReccurent(data)
                            });
                            break;
                        }
                        else
                            continue;
                    }
                    int size = reader.ReadStream<int>();
                    if (reader.CanRead<byte>(size))
                    {
                        var data = reader.ReadArray<byte>(size);
                        l.Add(new
                        {
                            id,
                            size,
                            data = ByteReccurent(data)
                        });
                    }
                    else
                        break;
                }
                else
                    break;
            }
            if (l.Any())
            {
                if (l.Count > 1)
                    return l.ToArray();
                return l.Last();
            }
        fail:
            return parse2(arr);
        }public static int CmpRecords(object[] list, int idx, int len)
        {
            int repeats = 1;
            bool onlyBytes = true;
            do
            {
                onlyBytes = false;
                for (int i = 0; i < len; i++)
                {
                    if (idx + i >= list.Length || idx + len * repeats + i >= list.Length)
                        return repeats;
                    var var1 = list[idx + i];
                    var var2 = list[idx + len * repeats + i];
                    if (var1.GetType() != var2.GetType())
                        return repeats;
                    onlyBytes &= var1 is byte[];
                    if (var1 is byte[])
                    {
                        var a1 = (byte[])var1;
                        var a2 = (byte[])var2;
                        if (a1.Length != a2.Length)
                            return repeats;
                    }
                }
            }
            while (!onlyBytes && repeats++ < 1000);
            return repeats;
        }
       static object fparse2(byte[] b)
       {
           var bf = new BitFinder();
           var List = new List<object>();
           var listarray = new List<byte>();
           var l1 = bf.Decode(b).ToList();
           object transform(object a)
           {
               return a;
               if (a is byte[])
               {
                   object[] rr = bf.Decode2((a as byte[])).ToArray();
                   if (rr.Length > 1)
                       return rr;
                   else
                       return (rr[0]);
               }
               else
                   return a;
           }
           foreach (var tt in bf.Decode(b))
           {
               if (tt is byte)
                   listarray.Add((byte)tt);
               else
               {
                   if (listarray.Count > 0)
                   {
                       List.Add(listarray.ToArray());
                       listarray = new List<byte>();
                   }
                   List.Add(tt);
               }
           }
           if (listarray.Any())
           {
               List.Add(listarray.ToArray());
               listarray.Clear();
           }
           var Larr = List.ToArray();
           var newList = new List<object>();
           for (int i = 0; i < List.Count;)
           {
               int pluser = 1;
               for (int rsize = 100; rsize > 1; rsize--)
               {
                   int rlen = CmpRecords(Larr, i, rsize);
                   if (rlen > 1)
                   {
                       var group = new List<object[]>();
                       for (int x = 0; x < rlen; x++)
                       {
                           var rlist = new List<object>();
                           for (int y = 0; y < rsize; y++)
                               rlist.Add(transform(Larr[i + x * rsize + y]));
                           group.Add(rlist.ToArray());
                       }
                       newList.Add(group.ToArray());
                       pluser = rlen * rsize;
                       goto l1;
                   }
               }
               newList.Add(transform(List[i]));
           l1:
               i += pluser;
           }
           object[] r = newList.ToArray();
           if (r.Length == 1)
               return r[0];
           else
               return r;
       }
       */
