﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
namespace BinSacnner
{
    public class Reader
    {
        public static Encoding enc1251 = Encoding.GetEncoding(1251);
        public byte[] arr;
        public bool Eof()
        {
            return currptr >= lastptr;
        }
        int lastptr = 0;

        public Reader(byte[] b, int ptr, int size) : this(b)
        {
            currptr = ptr;
            lastptr = ptr + lastptr;
        }
        protected int currptr = 0;
        int iterator<T>()
        {
            int ss = Utils.SizeOf<T>();
            currptr += ss;
            return currptr - ss;
        }
        public T[] ReadArray<T>(int size)
        {

            if (typeof(T) == typeof(byte))
            {
                try
                {
                    byte[] r = new byte[size];
                    for (int i = 0; i < size; i++)
                        r[i] = arr[currptr++];
                    return r as T[];

                }
                catch (Exception e)
                {

                }

            }

            T[] res = new T[size];
            for (int i = 0; i < size; i++)
                res[i] = ReadStream<T>();
            return res;
        }
        public T[] ReadArray<T>()
        {

            if (typeof(T) == typeof(byte))
                return ReadArray<T>(arr.Length - currptr);

            var list = new List<T>();
            while (!Eof())
                list.Add(ReadStream<T>());
            return list.ToArray();
        }
        public int Position { get { return currptr; } }
        // MethodInfo ReadArrayMethod = typeof(MegaReader).GetMethod("ReadArray");
        MethodInfo ReadStreamMethod = typeof(Reader).GetMethod("TryClass", new Type[0]);
        public T ReadStream<T>(int size)
        {
            var block = ReadArray<byte>(size);
            if (typeof(T) == typeof(object))
            {
                return (T)parse2(block);
            }
            else
            {
                return (new Reader(block)).ReadStream<T>();
            }
        }
        public T ReadChunker<T>()
        {
            var type = typeof(T);
            if (type == typeof(Razdelx))
                type = type;
            var ctor = type.GetConstructor(new Type[0]);
            var obj = ctor.Invoke(new object[0]);
            int num = 1;
            foreach (var p in type.GetProperties())
            {
                int id = ReadStream<short>();
                if (id == -1)
                {
                    break;
                }
                if (id != num++)
                {
                    throw new SystemException();
                }
                int size = ReadStream<int>();
                byte[] block = ReadArray<byte>(size);
                var reader = new Reader(block);
                MethodInfo method = typeof(Reader).GetMethod("ReadStream", new Type[0]);
                method = method.MakeGenericMethod(p.PropertyType);
                var res = method.Invoke(reader, new object[0]);
                // var val = (res as dynamic).body;
                p.SetValue(obj, res);
            }
            int sign = ReadStream<short>();
            if (sign != -1)
                throw new SystemException();
            return (T)Convert.ChangeType(obj, typeof(T));
        }


        Dictionary<Type, Delegate> methoddic = new Dictionary<Type, Delegate>();



        public Reader(byte[] b)
        {

            methoddic[typeof(int)] = (Func<int>)(() => BitConverter.ToInt32(arr, (currptr += 4) - 4));
            methoddic[typeof(uint)] = (Func<uint>)(() => BitConverter.ToUInt32(arr, (currptr += 4) - 4));
            methoddic[typeof(short)] = (Func<short>)(() => BitConverter.ToInt16(arr, (currptr += 2) - 2));
            methoddic[typeof(ushort)] = (Func<ushort>)(() => BitConverter.ToUInt16(arr, (currptr += 2) - 2));
            methoddic[typeof(double)] = (Func<double>)(() => BitConverter.ToDouble(arr, (currptr += 8) - 8));
            methoddic[typeof(float)] = (Func<float>)(() => BitConverter.ToSingle(arr, (currptr += 4) - 4));
            methoddic[typeof(byte)] = (Func<byte>)(() => arr[currptr++]);
            methoddic[typeof(string)] = (Func<string>)(() =>
                {
                    var size = BitConverter.ToUInt16(arr, currptr);
                    currptr += size + 2;
                    return enc1251.GetString(arr, currptr - size, size);
                }
            );
            methoddic[typeof(Guid)] = (Func<Guid>)(() => Guid.Parse(ReadStream<string>()));

            methoddic[typeof(Guid?)] = (Func<Guid?>)(() =>
            {
                Guid res;
                if (Guid.TryParse(ReadStream<string>(), out res))
                    return res;
                return null;
            }
            );

            methoddic[typeof(object)] = (Func<object>)(() =>
                   {
                       var block = ReadArray<byte>();
                       if (block.Length == 2)
                           return BitConverter.ToInt16(block, 0);
                       if (block.Length == 4)
                           return BitConverter.ToInt32(block, 0);
                       else
                           return parse2(block);
                   });

            arr = b;
            lastptr = arr.Length;
        }

        public T ReadStream<T>()
        {

            Type type = typeof(T);

            if (methoddic.ContainsKey(type))
                return (methoddic[type] as Func<T>)();
            else
            {
                /*
                IEnumerable<Type> list = Assembly.GetAssembly(type)
                .GetTypes().Where(t => t.BaseType == (type)).OrderBy(x => x.Name).ToList();*/

                var list = Streamer.children<T>();

                if (list.Any())
                {
                    foreach (Type subClass in list)
                    {
                        try
                        {
                            MethodInfo genericMethod = ReadStreamMethod.MakeGenericMethod(subClass);
                            object a = genericMethod.Invoke(this, null);
                            if (a != null)
                                return (T)a;
                        }
                        catch//(SystemException w)
                        {
                        }
                    }
                    throw new SystemException("ss");
                }
                return (T)Streamer.ReadClass<T>(this);
            }

        }
        object TryFloats<T>() where T : IComparable<T>
        {
            int size = Utils.SizeOf<T>();
            if (currptr + size >= lastptr)
                return null;
            dynamic val = ReadStream<T>();
            var s = val.ToString("0.########").Replace(',', '.').ToLower();
            if (Double.IsNaN(val) || Double.IsInfinity(val) || val > 10000000 ||
                val < 0.00001 || s.Contains("e") || s.Length > 9 || (s.Contains(".") && int.Parse(s.Split('.')[1]) > 1000))
            {
                currptr -= size;
                return null;
            }
            return val;
        }
        int savedPos;
        public void SavePosition()
        {
            savedPos = currptr;
        }
        public object RollBack()
        {
            currptr = savedPos;
            return null;
        }
        bool CanRead<T>(int count = 1)
        {
            if (count <= 0)
                return false;
            int size = Utils.SizeOf<T>();
            return !(currptr + size * count > lastptr);
        }
        object TryShort()
        {
            SavePosition();
            if (CanRead<short>())
            {
                short res = ReadStream<short>();
                if (res >= -1 && res < 20000)
                    return res;
            }
            return RollBack();
        }
        object TryInt()
        {
            SavePosition();
            if (CanRead<int>())
            {
                int res = ReadStream<int>();
                if (res >= -1 && res < 2000000)
                    return res;
            }
            return RollBack();
        }
        object TryArray<T>(int count)
        {
            if (CanRead<T>(count))
                return ReadArray<T>(count);
            else
                return null;
        }
        public object TryClass<T>()
        {
            SavePosition();
            try
            {
                var a = ReadStream<T>();
                if (a == null)
                    return RollBack();
                return a;
            }
            catch (Exception r)
            {
                return RollBack();
            }
        }
        object TryString()
        {
            SavePosition();
            if (CanRead<short>())
            {
                short sz = ReadStream<short>();
                if (CanRead<byte>(sz))
                {
                    byte[] ms = ReadArray<byte>(sz);
                    if (!ms.Where(curr => !((curr >= 32 && curr <= 128) || curr > 0xc0)).Any())
                        if (!(sz < 3 && ms.Where(curr => (curr >= 0x40 && curr <= 0x80)).Any()))
                            // if (sz>2)
                            return enc1251.GetString(ms);
                }
            }
            return RollBack();
        }
        object TryList()
        {
            SavePosition();
            short sign = 0;
            do
            {
                if (currptr + 2 > lastptr)
                    return RollBack();
                sign = ReadStream<short>();
                if (sign == -1)
                {
                    if (currptr - savedPos == 2)
                        return RollBack();
                    break;
                }
                else
                {
                    if (sign <= 0 || sign > 100 || currptr + 4 > lastptr)
                        return RollBack();
                    var size = ReadStream<int>();
                    if (size < 0 || currptr + size > lastptr)
                        return RollBack();
                    currptr += size;
                }
            } while (true);
            RollBack();
            List<object> list = new List<object>();
            while ((sign = ReadStream<short>()) != -1)
            {
                int size = ReadStream<int>();
                object data = parse2(ReadArray<byte>(size));
                list.Add(new { sign, size, data });
            }
            return new { list.Count, list };
        }

        public static IEnumerable<object> ParseStreamE(byte[] arr)
        {
            Reader reader = new Reader(arr);
            while (!reader.Eof())
            {
                object v = null;


                /*
                reader.SavePosition();
                if ((v = reader.TryString()) != null)
                {
                    if ((v as string).Length < 22 && (v as string).Where(x => x == '-' || x == '1' || x == '2').Count() > 0)
                    {
                        reader.RollBack();
                        if ((v = reader.TryClass<Resurs>()) == null)
                            // if ((v = reader.TryClass<Resurs2>()) == null)
                            //   if ((v = reader.TryClass<Resurs3>()) == null)
                            v = reader.ReadStream<string>();
                    }
                }
                else*/
                if ((v = reader.TryList()) == null)
                    if ((v = reader.TryString()) == null)
                        //             if (null == (v = reader.TryFloats<double>()))
                        v = reader.ReadStream<byte>();
                yield return v;
            }
        }
        public static IEnumerable<object> Packer(IEnumerable<object> bts)
        {
            var list = new List<byte>();
            object packByte(byte[] b)
            {
                /*
                if (b.Length == 2)
                    return BitConverter.ToInt16(b, 0);
                else
                if (b.Length == 4)
                    return BitConverter.ToInt32(b, 0);
                else
                if (b.Length == 1)
                    return b[0];
                else */
                return b;
            }
            foreach (var b in bts)
            {
                if (b is byte)
                    list.Add((byte)b);
                else
                {
                    if (list.Any())
                    {
                        yield return packByte(list.ToArray());
                        list.Clear();
                    }
                    yield return b;
                }
            }
            if (list.Any())
                yield return packByte(list.ToArray());
        }
        public static object parse2(byte[] arr)
        {
            var a = Packer(ParseStreamE(arr)).ToArray();
            if (a.Length == 1)
                return a[0];
            return a;
        }
    }
}
