﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
namespace BinSacnner
{
    public class ReaderComplex
    {
        public PropertyInfo p;
        public MethodInfo method;
        public object[] parameters;
        public string SizeFromProperty;
        public string DynamicClass;
    }
    public static class Streamer
    {
        public static object[] nullPtr = new object[] { };
        static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public static List<Type> children<T>()
        {
            return reads<T>.Children;
        }

        public static object ReadClass<T>(Reader reader)
        {
            object obj = reads<T>.create();
            if (typeof(T).GetCustomAttribute<ChunkerAttribute>() != null)
            {
                var type = typeof(T);
                if (type == typeof(Razdelx))
                    type = type;
                short sign = 0;
                int num = 0;
                while ((sign = reader.ReadStream<short>()) != -1)
                {
                    if (num++ > 100)
                        num--;
                    if (sign <= reads<T>.mds.Count)
                    {
                        int n = sign - 1;
                        if (n < reads<T>.mds.Count)
                        {
                            var methods = reads<T>.mds[n];
                            int size = reader.ReadStream<int>();
                            byte[] block = reader.ReadArray<byte>(size);
                            var reader2 = new Reader(block);
                            object val = null;
                            try
                            {
                                val = methods.method.Invoke(reader2, methods.parameters);
                            }
                            catch (Exception e)
                            {

                            }
                            // var val = (res as dynamic).body;
                            methods.p.SetValue(obj, val);
                        }
                    }
                }
                /*
                foreach (var methods in reads<T>.mds)
                {
                    reader.SavePosition();
                    sign = reader.ReadStream<short>();
                    if (sign == -1)
                    {
                        break;
                    }
                    if (sign != num++)
                    {
                      //  reader.RollBack();
                       // return obj;
                       // throw new SystemException();
                    }
                    int size = reader.ReadStream<int>();
                    byte[] block = reader.ReadArray<byte>(size);
                    var reader2 = new MegaReader(block);
                    object val = null;
                    try
                    {
                         val = methods.method.Invoke(reader2, methods.parameters);
                    }
                    catch(Exception e)
                    {
                    }
                    // var val = (res as dynamic).body;
                    methods.p.SetValue(obj, val);
                }
                */
                while (sign != -1)
                {
                    sign = reader.ReadStream<short>();
                    if (sign == -1)
                        break;
                    var size = reader.ReadStream<int>();
                    var body = reader.ReadArray<byte>(size);
                }
                //throw new SystemException();
            }
            else
                foreach (var methods in reads<T>.mds)
                {
                    if (reader.Eof())
                        break;

                    object val;
                    var method = methods.method;

                    if (methods.DynamicClass != null)
                    {
                        MethodInfo d = typeof(T).GetMethod(methods.DynamicClass);
                        Type res = d.Invoke(obj, new object[0]) as Type;
                        method = reads<T>.GetMethodInfo(methods.p, res);
                        //  val = method.Invoke(reader, methods.parameters);
                    }

                    if (methods.SizeFromProperty == null)
                    {

                        val = method.Invoke(reader, methods.parameters);

                    }
                    else
                    {
                        object osize = GetPropValue(obj, methods.SizeFromProperty);

                        int size = 0;
                        if (osize is int)
                            size = (int)(osize);
                        else
                        if (osize is uint)
                            size = (int)(uint)(osize);
                        else
                        if (osize is short)
                            size = (short)(osize);
                        else
                        if (osize is ushort)
                            size = (ushort)(osize);
                        else
                        if (osize is byte)
                            size = (byte)(osize);

                        if (size > 100)
                            throw new SystemException("sss");


                        val = method.Invoke(reader, new object[] { size });
                    }
                    if (val == null)
                        return null;


                    methods.p.SetValue(obj, val);
                }
            if (typeof(T) == typeof(Res))
                obj = obj;
            return obj;
        }
        private static class reads<T>
        {
            public static ConstructorInfo ctor;
            public static List<ReaderComplex> mds = new List<ReaderComplex>();

            public static List<Type> Children = new List<Type>();

            public static object create()
            {
                return ctor.Invoke(nullPtr);
            }

            public static MethodInfo GetMethodInfo(PropertyInfo p, Type PropertyType)
            {
                MethodInfo method = typeof(Reader).GetMethod(
                       PropertyType.IsArray ?
                       "ReadArray" :
                       "ReadStream",
                      (p.GetCustomAttribute<SizeAttribute>() != null || p.GetCustomAttribute<SizeFormPropertyAttribute>() != null) ?
                      new Type[] { typeof(int) } : new Type[0]);
                var ptrs = (p.GetCustomAttribute<SizeAttribute>() != null ||
                            p.GetCustomAttribute<SizeFormPropertyAttribute>() != null) ?
                            new Type[] { typeof(int) } :
                            new Type[0];
                method = PropertyType.IsArray ?
                method.MakeGenericMethod(PropertyType.GetElementType()) :
                method.MakeGenericMethod(PropertyType);

                return method;
            }

            static reads()
            {
                var type = typeof(T);

                Children = Assembly.GetAssembly(type).GetTypes().Where(t => t.BaseType == (type)).OrderBy(x => x.Name).ToList();


                ctor = type.GetConstructor(new Type[0]);
                foreach (var p in type.GetProperties())
                {
                    var parameters = p.GetCustomAttribute<SizeAttribute>() != null ?
                      new object[] { p.GetCustomAttribute<SizeAttribute>().Size } : new object[0];


                    string SizeFromProperty = p.GetCustomAttribute<SizeFormPropertyAttribute>() != null ?
                                      p.GetCustomAttribute<SizeFormPropertyAttribute>().Name :
                                      null;

                    string DynamicClass = p.GetCustomAttribute<DynamicClassAttribute>() != null ?
                                  p.GetCustomAttribute<DynamicClassAttribute>().Name :
                                  null;


                    if (p.Name == "QQQ")
                        SizeFromProperty = SizeFromProperty;

                    MethodInfo method = GetMethodInfo(p, p.PropertyType);

                    mds.Add(new ReaderComplex()
                    {
                        parameters = parameters,
                        SizeFromProperty = SizeFromProperty,
                        DynamicClass = DynamicClass,
                        method = method,
                        p = p
                    });
                }
                /*
                mds = type.GetProperties().Select(p =>
                new ReaderComplex
                {
                    parameters = p.GetCustomAttribute<SizeAttribute>() != null ?
                      new object[] { p.GetCustomAttribute<SizeAttribute>().Size } : new object[0],
                    p = p,
                    method = p.PropertyType.IsArray ?
                    ReadArrayMethod.MakeGenericMethod(p.PropertyType.GetElementType()) :
                    ReadStreamMethod.MakeGenericMethod(p.PropertyType),
                    SizeFromProperty = p.GetCustomAttribute<SizeFormPropertyAttribute>() != null ?
                                       p.GetCustomAttribute<SizeFormPropertyAttribute>().Name :
                                       null
                }).ToList();*/
            }
        }
    }
}
