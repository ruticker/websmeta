﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BinSacnner;

namespace BinSacnner
{
    class TreeList
    {
        public string Name;
        public List<TreeList> children = new List<TreeList>();
        public static TreeList FromBaser(Baser data )
        {
            if (data is ChuncksZBody)
            {
                var res = new TreeList();
                var d = data as ChuncksZBody;
                res.Name = d.Part1.Header.paragraph +" "+ d.Part1.Header.name;
                foreach(var t in d.Part1.Razdels)
                {
                    res.children.Add(FromBaser(t.Body));
                }
                return res;
            }
            else
            {
                var res = new TreeList();
                var d = data as Razdelx;
                res.Name =  d.s1s1.Name;
                return res;
            }
        }
    }
}
