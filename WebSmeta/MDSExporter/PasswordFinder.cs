﻿using System.Collections.Generic;
using System.Text;
namespace MDSExporter
{
    class PasswordFinder
    {
        byte[] bytes;
        public Encoding enc1251 = Encoding.GetEncoding(1251);
        public PasswordFinder(byte[] b)
        {
            bytes=b;
        }
        public IEnumerable<string> FindAll(int minl, int maxl)
        {
            int l = bytes.Length;
            for (int i = 0; i<l-maxl; i++)
                for (int j = 0; j<maxl-1; j++)
                {
                    byte curr = bytes[i+j];
                    if (curr==0&&j>minl)
                    {
                        yield return enc1251.GetString(bytes, i, j);
                        i+=j;
                    }
                    if (!((curr>=32&&curr<=128)||curr>0xc0))
                        break;
                }
        }
    }
}
public class Rootobject
{
    public Class1[] Property1 { get; set; }
}
public class Class1
{
    public string OBOSNOV { get; set; }
    public RS RS { get; set; }
}
public class RS
{
    public string Description { get; set; }
    public int CODE { get; set; }
    public string Metric { get; set; }
    public string[] Works { get; set; }
    public string Law { get; set; }
    public Subwork[] Subworks { get; set; }
}
public class Subwork
{
    public string Obosnov { get; set; }
    public int Type { get; set; }
    public float Value { get; set; }
}
