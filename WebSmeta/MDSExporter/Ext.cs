﻿using System;
using FirebirdSql.Data.FirebirdClient;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.IO;
using System.Text;
namespace MDSExporter
{
    public static class Ext
    {
        public static int ReadInt(this MemoryStream stream)
        {
            int res = 0;
            for (int i = 0; i<32; i+=8)
                res|=((int)stream.ReadByte())<<i;
            return res;
        }
        public static ushort ReadShort(this MemoryStream stream)
        {
            ushort res = 0;
            for (ushort i = 0; i<16; i+=8)
                res|= (ushort)( ((ushort)stream.ReadByte())<<i);
            return res;
        }
        public static void Skip(this MemoryStream stream, int i)
        {
            stream.Seek(i, SeekOrigin.Current);
        }
        public static string ReadString(this MemoryStream stream)
        {
            var len=stream.ReadShort();
            byte[] b = new byte[len];
            for (int i = 0; i<len; i++)
                b[i]= (byte)stream.ReadByte();
            return Encoding.GetEncoding(1251).GetString(b);
        }
        public static string ReadStr(this BinaryReader stream)
        {
            if (stream.BaseStream.Position-stream.BaseStream.Length==-1)
                return "";
            var len = stream.ReadUInt16();
            byte[] b = new byte[len];
            for (int i = 0; i<len; i++)
                b[i]=(byte)stream.ReadByte();
            return Encoding.GetEncoding(1251).GetString(b);
        }
        public static bool EOF(this BinaryReader binaryReader)
        {
            var bs = binaryReader.BaseStream;
            return (bs.Position==bs.Length);
        }
        public static bool ContainsAllCase(this string s1, string s2)
        {
            return s1.ToUpper().Contains(s2.ToUpper());
        }
        public static byte[] ReadAllBytes(this BinaryReader reader)
        {
            const int bufferSize = 4096;
            using (var ms = new MemoryStream())
            {
                byte[] buffer = new byte[bufferSize];
                int count;
                while ((count=reader.Read(buffer, 0, buffer.Length))!=0)
                    ms.Write(buffer, 0, count);
                return ms.ToArray();
            }
        }
        public static IEnumerable<int> StartingIndex(this byte[] x, byte[] y)
        {
            if (x.Length>=y.Length)
            {
                IEnumerable<int> index = Enumerable.Range(0, x.Length-y.Length+1);
                for (int i = 0; i<y.Length; i++)
                {
                    index=index.Where(n => x[n+i]==y[i]).ToArray();
                }
                return index;
            }
            return new int[0];
        }
        public static byte[] HexToByteArray(this string hex)
        {
            string h = hex.Replace(" ", "").Replace("-", "");
            byte[] y = Enumerable.Range(0, h.Length)
                             .Where(xx => xx%2==0)
                             .Select(xx => Convert.ToByte(h.Substring(xx, 2), 16))
                             .ToArray();
            return y;
        }
        public static IEnumerable<int> StartingIndex(this byte[] x, string hex)
        {
            byte[] y = hex.HexToByteArray();
            if (x.Length>=y.Length)
            {
                IEnumerable<int> index = Enumerable.Range(0, x.Length-y.Length+1);
                for (int i = 0; i<y.Length; i++)
                {
                    index=index.Where(n => x[n+i]==y[i]).ToArray();
                }
                return index;
            }
            return new int[0];
        }
    }
}
