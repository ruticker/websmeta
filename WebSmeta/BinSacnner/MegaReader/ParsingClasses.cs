﻿using System;
namespace BinSacnner
{
  //"Stree1": [ "19-00-00-00-15-00-01-00-00-00-03-00-00-00-02", "Бульдозеры, мощность 79 кВт (108 л.с.)", "9101-1-35", "00-00", "маш.-ч", "00-00-00-00 -00-00-00-00- 00-00-00-00-00-00", "-1.0*(2,5 * 10,27)", "CC-CC-CC-CC- CC-AC-39-C0-   00-00-00-00-00-00-00-00 -00-00-00-00-00-00-00-00  -00-00-00-00- CC-CC-CC-CC-CC-AC-39-C0-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00", 79.07, 79.07, 13.5, 1, 4, "88-B7-32-01-00-00", " (исключается стоимость ресурса)", "00" ],
    public class stree1class
    {
      public  int i1 { get; set; }
        public short s1 { get; set; }
        public int i2 { get; set; }
        public int i3 { get; set; }
        public byte b1 { get; set; }
        public string Name { get; set; }
        public string Cod { get; set; }
        public string s2 { get; set; }
        public string rsize { get; set; }
        public string sostav { get; set; }
        public string ss1 { get; set; }
        public string ss2 { get; set; }
        public string ss3 { get; set; }
        public string ss4 { get; set; }
        public string ss5 { get; set; }
        public string ss6 { get; set; }
        public string mnoj { get; set; }
        public double dub { get; set; }
        [Size(5)]
        public int[] ints { get; set; }
        public double dub2 { get; set; }
        [Size(6)]
        public int[] ints2 { get; set; }
        [Size(5)]
        public double[] dubs { get; set; }
        public int finish { get; set; }
        public string finish1 { get; set; }
        public string finish2 { get; set; }
        /* public double DD1 { get; set; }
         public double DD2 { get; set; }
         public string formula { get; set; }
         public double DD3 { get; set; }*/
        public byte unkyyynown { get; set; }
    }
    public class ArrayWrapper<T>
    {
        public T[] value { get; set; }
    }
    /*
    public class Chunk<T>
    {
        public short id { get; set; }
        public int size { get; set; }
        [SizeFormProperty("size")]
        public T body { get; set; }
    }
    */
    public class Header
    {
        public int num1 { get; set; }
        public int num2 { get; set; }
        public int num3 { get; set; }
        public int num4 { get; set; }
        public string number { get; set; }
        public string name { get; set; }
        public short someZero { get; set; }
    }
    [Chunker]
    public class BlockContainer
    {
        public byte[] Part1 { get; set; }
        public byte[] Part2 { get; set; }
        public byte[] Part3 { get; set; }
    }
    /*
    public class Chuncks<T>
    {
        public Chunk<T> Part1 { get; set; }
        public Chunk<byte> Part2 { get; set; }
        public Chunk<int> Part3 { get; set; }
        public Chunk<BlockContainer> Part4 { get; set; }
        public Chunk<int> Part5 { get; set; }
        public Chunk<Chunk<int>> Part6 { get; set; }
        public Chunk<Guid> Part7 { get; set; }
        public short SignMinusOne { get; set; }
    }*/
    [Chunker]
    public class IntContainer
    {
        public int val { get; set; }
    }
    [Chunker]
    public class ChuncksZ<T>
    {
        public T Part1 { get; set; }
        public byte Part2 { get; set; }
        public int Part3 { get; set; }
        public BlockContainer Part4 { get; set; }
        public int Part5 { get; set; }
        public IntContainer Part6 { get; set; }
        public Guid Part7 { get; set; }
    }
    public class ElevenBlock
    {
        public double param1 { get; set; }
        public short param2 { get; set; }
        public byte param3 { get; set; }
    }
    public class SeventeenBlock
    {
        public double param1 { get; set; }
        public double param2 { get; set; }
        public byte param3 { get; set; }
    }
    public class Block13
    {
        public double param1 { get; set; }
        public int param2 { get; set; }
        public byte param3 { get; set; }
    }
    public class Segment1
    {
        public Header Header { get; set; }
        public MagicBlock magicBlock { get; set; }
        // public short sign { get; set; }
        public int size { get; set; }
        public int zero { get; set; }
        [SizeFormProperty("size")]
        public SmetaBlock[] SBlock { get; set; }
    }
    public class SmetaBlock
    {
        public ChuncksZ<body> Body { get; set; }
        //  public short minus { get; set; }
        public int ZeroIntDelimeter { get; set; }
    }
    public class body
    {
        public Header Header { get; set; }
        public MagicBlock magicBlock { get; set; }
       
         public int size { get; set; }

         [SizeFormProperty("size")]
         public Razdel[] Razdels { get; set; }
         public byte[] last { get; set; }
    }
    public class Razdel
    {
        public int zeroZ { get; set; }
      //  public short zeroZ1 { get; set; }
        public Razdelx razdel { get; set; }
    }
    [Chunker]
    public class Razdelx
    {
         public stree1class s1s1 { get; set; }
         public RES Stree2 { get; set; }
         public MagicBlock Stree3 { get; set; }
         public object Stree4 { get; set; }
         public object Stree5 { get; set; }
         public byte S1 { get; set; }
         public object S2 { get; set; }
         public object S3 { get; set; }
         public object S4 { get; set; }
         public  s5class  S5 { get; set; }
         public object S6 { get; set; }
         public object S7 { get; set; }
         public byte S8 { get; set; }
         public s9class S9 { get; set; }
         public string Guid10 { get; set; }
         public BlockContainer S12 { get; set; }
         public object S13 { get; set; }
         public object S14 { get; set; }
         public s15class S15 { get; set; }
         public object S16 { get; set; }
         public object S17 { get; set; }
         public Guid S18 { get; set; }
         public object S19 { get; set; }
         public object S20 { get; set; }
         public object S21 { get; set; }
    }
    [Chunker]
    public class s9class
    {
        public object o1 { get; set; }
        public ArrayWrapper<double> o2 { get; set; }
        public double o3 { get; set; }
        public double o4 { get; set; }
        public ArrayWrapper<byte> o5 { get; set; }
    }
    [Chunker]
    public class s15class
    {
        public object o1 { get; set; }
        public string fed { get; set; }
    }
    public class s34wp
    {
        public int i1 { get; set; }
        public int i2 { get; set; }
        public int ix3 { get; set; }
    }
    [Chunker]
    public class s34class
    {
        public object o1 { get; set; }
        public object o2 { get; set; }
        public object o3 { get; set; }
        public object o4 { get; set; }
        public object o5 { get; set; }
        public object o6 { get; set; }
        public object o7 { get; set; }
        public object o8 { get; set; }
        public object o9 { get; set; }
        public object oA { get; set; }
    }
    [Chunker]
    public class s5class
    {
        public ArrayWrapper<double> o1 { get; set; }
        public ArrayWrapper<double> o2 { get; set; }
    }
    [Chunker]
    public class s5class1
    {
        public ArrayWrapper<double> o1 { get; set; }
        public ArrayWrapper<double> o2 { get; set; }
    }
    [Chunker]
    public class MagicBlock
    {
        public ArrayWrapper<ElevenBlock> EW1 { get; set; }
        public int i1 { get; set; }
        public ArrayWrapper<ElevenBlock> EW2 { get; set; }
        public ArrayWrapper<ElevenBlock> EW3 { get; set; }
        public ArrayWrapper<ElevenBlock> EW4 { get; set; }
        public ArrayWrapper<ElevenBlock> EW5 { get; set; }
        public ArrayWrapper<ElevenBlock> EW6 { get; set; }
        public byte[] Data1 { get; set; }
        public ArrayWrapper<Block13> W13_1 { get; set; }
        public ArrayWrapper<Block13> W13_2 { get; set; }
        public ArrayWrapper<Block13> W13_3 { get; set; }
        public ArrayWrapper<Block13> W13_4 { get; set; }
        public object Unk1 { get; set; }
        public int Unk2 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW11 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW12 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW13 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW14 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW15 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW16 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW17 { get; set; }
        public ArrayWrapper<SeventeenBlock> SW18 { get; set; }
        public object Last { get; set; }
    }
    public class RES
    {
        public int count { get; set; }
        [SizeFormProperty("count")]
        public Res[] fields { get; set; }
    }
    public class Q1
    {
        public string COD { get; set; }
        public string Name { get; set; }
        public string Rsize { get; set; }
        [Size(10)]
        public double[] values { get; set; }
        public  byte b1 { get; set; }
        public byte b2 { get; set; }
        public int i1 { get; set; }
        public short s1 { get; set; }
        public double d1 { get; set; }
        public double d2 { get; set; }
        public double d3 { get; set; }
        public object last { get; set; }
    }
    [Chunker]
    public class Res
    {
        public Q1 q1 { get; set; }
        public object q2 { get; set; }
        public object q3 { get; set; }
        public int q4 { get; set; }
        public object q5 { get; set; }
        public s5class1 q6 { get; set; }
        public short q7 { get; set; }
        public string guid1 { get; set; }
        public string q9 { get; set; }
        public s15class q10 { get; set; }
        public int q11 { get; set; }
        public Guid guid2 { get; set; }
        public object q13 { get; set; }
        public short q14 { get; set; }
        public byte q15 { get; set; }
        public short q16 { get; set; }
    }
}
// Razdelx 19/20  res - 15/16
/*
public class Res
{
  [Size(16)]
  public Chunk<object>[] q1 { get; set; }
  public short signm { get; set; }
}*/
/*
public class MagicBlock
{
    public Chunk<ArrayWrapper<ElevenBlock>> EW1 { get; set; }
    public Chunk<int> i1 { get; set; }
    public Chunk<ArrayWrapper<ElevenBlock>> EW2 { get; set; }
    public Chunk<ArrayWrapper<ElevenBlock>> EW3 { get; set; }
    public Chunk<ArrayWrapper<ElevenBlock>> EW4 { get; set; }
    public Chunk<ArrayWrapper<ElevenBlock>> EW5 { get; set; }
    public Chunk<ArrayWrapper<ElevenBlock>> EW6 { get; set; }
    public Chunk<byte[]> Data1 { get; set; }
    public Chunk<ArrayWrapper<Block13>> W13_1 { get; set; }
    public Chunk<ArrayWrapper<Block13>> W13_2 { get; set; }
    public Chunk<ArrayWrapper<Block13>> W13_3 { get; set; }
    public Chunk<ArrayWrapper<Block13>> W13_4 { get; set; }
    public Chunk<object> Unk1 { get; set; }
    public Chunk<int> Unk2 { get; set; }
    [Size(8)]
    public Chunk<ArrayWrapper<SeventeenBlock>>[] SW1 { get; set; }
    public Chunk<object> Last { get; set; }
}*/
/*
public class Razdelx
{
  public Chunk<object> Stree1 { get; set; }
  public Chunk<RES> Stree2 { get; set; }
  public Chunk<MagicBlock> Stree3 { get; set; }
  public Chunk<ArrayWrapper<byte>> Stree4 { get; set; }
  public Chunk<ArrayWrapper<double>> Stree5 { get; set; }
  [Size(20)]
  public Chunk<object>[] Stree { get; set; }
  public short sign { get; set; }
//  public int size { get; set; }
}
*/
