﻿using System;
using System.Collections.Generic;
using System.Reflection;
namespace BinSacnner
{
    public class ReaderComplex
    {
        public PropertyInfo p;
        public MethodInfo method;
        public object[] parameters;
        public string SizeFromProperty;
    }
    public static class Streamer
    {
        public static object[] nullPtr = new object[] { };
        static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }
        public static object ReadClass<T>(MegaReader reader)
        {
            object obj = reads<T>.create();
            if (typeof(T).GetCustomAttribute<ChunkerAttribute>() != null)
            {
                var type = typeof(T);
                if (type == typeof(Razdelx))
                    type = type;
                short sign=0;
                int num = 0;
                while ((sign = reader.ReadStream<short>())!=-1)
                {
                    if (num++ > 100)
                        num--;
                    if (sign<= reads<T>.mds.Count)
                    {
                        int n = sign - 1;
                        if (n < reads<T>.mds.Count)
                        {
                            var methods = reads<T>.mds[n];
                            int size = reader.ReadStream<int>();
                            byte[] block = reader.ReadArray<byte>(size);
                            var reader2 = new MegaReader(block);
                            object val = null;
                            try
                            {
                                val = methods.method.Invoke(reader2, methods.parameters);
                            }
                            catch (Exception e)
                            {
                            }
                            // var val = (res as dynamic).body;
                            methods.p.SetValue(obj, val);
                        }
                    }
                }
                /*
                foreach (var methods in reads<T>.mds)
                {
                    reader.SavePosition();
                    sign = reader.ReadStream<short>();
                    if (sign == -1)
                    {
                        break;
                    }
                    if (sign != num++)
                    {
                      //  reader.RollBack();
                       // return obj;
                       // throw new SystemException();
                    }
                    int size = reader.ReadStream<int>();
                    byte[] block = reader.ReadArray<byte>(size);
                    var reader2 = new MegaReader(block);
                    object val = null;
                    try
                    {
                         val = methods.method.Invoke(reader2, methods.parameters);
                    }
                    catch(Exception e)
                    {
                    }
                    // var val = (res as dynamic).body;
                    methods.p.SetValue(obj, val);
                }
                */
                while (sign != -1)
                {
                    sign = reader.ReadStream<short>();
                    if (sign == -1)
                        break;
                    var size = reader.ReadStream<int>();
                    var body = reader.ReadArray<byte>(size);
                }
                //throw new SystemException();
            }
            else
                foreach (var methods in reads<T>.mds)
                {
                    object val;
                    if (methods.SizeFromProperty == null)
                    {
                        val = methods.method.Invoke(reader, methods.parameters);
                    }
                    else
                    {
                        object osize = GetPropValue(obj, methods.SizeFromProperty);
                        int size = 0;
                        if (osize is int)
                            size = (int)(osize);
                        else
                        if (osize is uint)
                            size = (int)(uint)(osize);
                        else
                        if (osize is short)
                            size = (short)(osize);
                        else
                        if (osize is ushort)
                            size = (ushort)(osize);
                        else
                        if (osize is byte)
                            size = (byte)(osize);
                        val = methods.method.Invoke(reader, new object[] { size });
                    }
                    if (val == null)
                        return null;
                    methods.p.SetValue(obj, val);
                }
            if (typeof(T) == typeof(Res))
                obj = obj;
            return obj;
        }
        private static class reads<T>
        {
            public static ConstructorInfo ctor;
            public static List<ReaderComplex> mds = new List<ReaderComplex>();
            public static object create()
            {
                return ctor.Invoke(nullPtr);
            }
            static reads()
            {
                var type = typeof(T);
                ctor = type.GetConstructor(new Type[0]);
                foreach (var p in type.GetProperties())
                {
                    var parameters = p.GetCustomAttribute<SizeAttribute>() != null ?
                      new object[] { p.GetCustomAttribute<SizeAttribute>().Size } : new object[0];
                    string SizeFromProperty = p.GetCustomAttribute<SizeFormPropertyAttribute>() != null ?
                                      p.GetCustomAttribute<SizeFormPropertyAttribute>().Name :
                                      null;
                    MethodInfo method = typeof(MegaReader).GetMethod(
                        p.PropertyType.IsArray ?
                        "ReadArray" :
                        "ReadStream",
                       (p.GetCustomAttribute<SizeAttribute>() != null || p.GetCustomAttribute<SizeFormPropertyAttribute>() != null) ?
                       new Type[] { typeof(int) } : new Type[0]);
                    var ptrs = (p.GetCustomAttribute<SizeAttribute>() != null ||
                                p.GetCustomAttribute<SizeFormPropertyAttribute>() != null) ?
                                new Type[] { typeof(int) } :
                                new Type[0];
                    method = p.PropertyType.IsArray ?
                    method.MakeGenericMethod(p.PropertyType.GetElementType()) :
                    method.MakeGenericMethod(p.PropertyType);
                    mds.Add(new ReaderComplex()
                    {
                        parameters = parameters,
                        SizeFromProperty = SizeFromProperty,
                        method = method,
                        p = p
                    });
                }
                /*
                mds = type.GetProperties().Select(p =>
                new ReaderComplex
                {
                    parameters = p.GetCustomAttribute<SizeAttribute>() != null ?
                      new object[] { p.GetCustomAttribute<SizeAttribute>().Size } : new object[0],
                    p = p,
                    method = p.PropertyType.IsArray ?
                    ReadArrayMethod.MakeGenericMethod(p.PropertyType.GetElementType()) :
                    ReadStreamMethod.MakeGenericMethod(p.PropertyType),
                    SizeFromProperty = p.GetCustomAttribute<SizeFormPropertyAttribute>() != null ?
                                       p.GetCustomAttribute<SizeFormPropertyAttribute>().Name :
                                       null
                }).ToList();*/
            }
        }
    }
}
