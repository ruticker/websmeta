﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
namespace EFGetStarted.AspNetCore.NewDb.Models
{
    public class SmetaContext : DbContext
    {
        const string connection =
            @"Server=DESKTOP-N6NCIMD\SQLEXPRESS;Database=NewDb;Trusted_Connection=True;ConnectRetryCount=0";
        public SmetaContext(DbContextOptions<SmetaContext> options)
            : base(options)
        {
        }
        static DbContextOptions<SmetaContext> param()
        {
            var optionsBuilder = new DbContextOptionsBuilder<SmetaContext>();
            optionsBuilder.UseSqlServer(connection);
            return optionsBuilder.Options;
        }
        public SmetaContext() : this(param())
        {
        }
        public DbSet<Code> Codes { get; set; }
        public DbSet<WorkConsist> WorkConsists { get; set; }
        public DbSet<Subwork> Subworks { get; set; }
        public DbSet<Resourse> Resourses { get; set; }
        public DbSet<Coeficient> Coeficients { get; set; }
        public DbSet<Cost> Costs { get; set; }
        public DbSet<Law> Laws { get; set; }
        public DbSet<ListBook> ListBooks { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Chapter> Chapter { get; set; }
        //    public DbSet<CoeficientCode> CoeficientCodes { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseSqlServer(connection);
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CodeCoeficient>()
                .HasKey(t => new { t.CodeId, t.CoeficientId });
            modelBuilder.Entity<CodeCoeficient>()
                .HasOne<Code>(sc => sc.Code)
                 .WithMany(s => s.CodeCoeficients)
               .HasForeignKey(sc => sc.CodeId);
            modelBuilder.Entity<ChapterCode>()
               .HasKey(t => new { t.ChapterId, t.CodeId });
            modelBuilder.Entity<ChapterCode>()
                .HasOne<Chapter>(sc => sc.Chapter)
                 .WithMany(s => s.ChapterCodes)
               .HasForeignKey(sc => sc.ChapterId);
            /* modelBuilder.Entity<CoeficientCode>()
                 .HasOne<Coeficient>(sc => sc.Coeficient)
                  .WithMany(s => s.CoeficientCodes)
                .HasForeignKey(sc => sc.CodeId);*/
        }
    }
    public class Book
    {
        public int BookId { get; set; }
    //    public int TPBASE { get; set; }
     //   public int TPBOOK { get; set; }
        public string OBOSNOV { get; set; }
        public string NAME { get; set; }
        public string COD { get; set; }
        public string SOKROBOZ { get; set; }
        public int DEFINITION { get; set; }
        public int ChapterId { get; set; }
        public Chapter Chapter { get; set; }
        public int ListBookId { get; set; }
        public ListBook ListBook { get; set; }
    }
    public class Chapter
    {
        public int ChapterId { get; set; }
     //   public int? ChapterId1 { get; set; }
        public string NAME { get; set; }
        public string COD { get; set; }
        public int? ParentId { get; set; }
        public virtual Chapter Parent { get; set; }
        public ICollection<Chapter> Chapters { get; set; }
        public ICollection<ChapterCode> ChapterCodes { get; } = new List<ChapterCode>();
    }
    public class ChapterCode
    {
        public int ChapterId { get; set; }
        public Chapter Chapter { get; set; }
        public int CodeId { get; set; }
        public Code Code { get; set; }
    }
    public class ListBook
    {
        public int ListBookId { get; set; }
        public int TPBASE { get; set; }
        public int TPBOOK { get; set; }
        public string NAME { get; set; }
        public string USL { get; set; }
        public ICollection<Book> Books { get; set; }
    }
    public class Law
    {
        public short LawId { get; set; }
        public string Name { get; set; }
    }
    public class Code
    {
        public int CodeId { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public Cost Cost { get; set; }
        public Resourse Resourse { get; set; }
        public ICollection<CodeCoeficient> CodeCoeficients { get; } = new List<CodeCoeficient>();
        public ICollection<ChapterCode> ChapterCodes { get; } = new List<ChapterCode>();
        //   public ICollection<Coeficient> Coeficients { get; } = new List<Coeficient>();
    }
    public class WorkConsist
    {
        public int WorkConsistId { get; set; }
        public string Name { get; set; }
        public int ResourseId { get; set; }
        [JsonIgnore]
        public Resourse Resourse { get; set; }
    }
    public class Subwork
    {
        public int SubworkId { get; set; }
        public int CodeId { get; set; }
        public Code Code { get; set; }
        public int Type { get; set; }
        public float Value { get; set; }
        public int? ResourseId { get; set; }
        public Resourse Resourse { get; set; }
    }
    public class Resourse
    {
        public int ResourseId { get; set; }
        public string NAME { get; set; }
        public string RSIZE { get; set; }
        public string Info { get; set; }
        public int Type { get; set; }
      
        public double DoubleCoef { get; set; }
        public string Works { get; set; }
        public short LawId { get; set; }
        public Law Law { get; set; }
        public ICollection<Subwork> Subworks { get; set; }
        public int CodeId { get; set; }
        public Code Code { get; set; }
    }
    public class Coeficient
    {
        public int CoeficientId { get; set; }
        public string POPR { get; set; }
        public string NAME { get; set; }
        public string NUMBER { get; set; }
        public string NAMEP { get; set; }
        public float? Labor { get; set; }
        public float? Machine { get; set; }
        public float? Material { get; set; }
    }
    public class CodeCoeficient
    {
        public int CodeId { get; set; }
        public Code Code { get; set; }
        public int CoeficientId { get; set; }
        public Coeficient Coeficient { get; set; }
    }
    public class Cost
    {
        public int CostId { get; set; }
        public string OBOSNOV { get; set; }
        public int REGION { get; set; }
        public float? Smetnaya { get; set; }
        public float? Otpusk { get; set; }
        public float? Total { get; set; }
        public float? LaborCost { get; set; }
        public float? Machine { get; set; }
        public float? Salary { get; set; }
        public float? Materials { get; set; }
        public float? Labor { get; set; }
        public float? Machinist { get; set; }
        public float? Price { get; set; }
        public int CodeId { get; set; }
        public Code Code { get; set; }
    }
}