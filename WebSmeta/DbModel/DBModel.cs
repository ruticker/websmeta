﻿using EFGetStarted.AspNetCore.NewDb.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
namespace DbModel
{
    public class DbModelTabs
    {
        public List<BOOK> BOOKS { get; set; }
        public List<LISTBOOK> LISTBOOKS { get; set; }
        public List<Listpopravki> Listpopravki { get; set; }
        public List<STable> Costs { get; set; }
        public Dictionary<string, RS> CostDic { get; set; }
        //   public Dictionary<string, Listpopravki> ListpopravkiDic { get; set; }
        public Dictionary<string, Poprav> Poprav { get; set; }
        //public List<Cost> Costs { get; set; }
        public List<P0> P0 { get; set; }
        public Dictionary<string, Task> Tasks { get; set; }
        public Dictionary<string, Material> Materials { get; set; }
        public Dictionary<string, Transport> Transports { get; set; }
        List<T> Deserialize<T>(string tab)
        {
            using (StreamReader file = File.OpenText(@"c:/db1/201017/" + tab))
            {
                JsonSerializer serializer = new JsonSerializer();
                return (List<T>)serializer.Deserialize(file, typeof(List<T>));
            }
        }
        int ArrIdx(string[] arr, int i)
        {
            if (i >= arr.Length)
                return 0;
            else
                return int.Parse(arr[i]);
        }
        public Chapter TreeToChapter(TitleTree tree)
        {
            return new Chapter
            {
                NAME = tree.NAME == null ? null : tree.NAME.Trim(),
                COD = tree.COD == null ? null : tree.COD.Trim(),
                Chapters = tree.CHAPTER == null ? new Chapter[0] : tree.CHAPTER.Select(x => TreeToChapter(x)).ToArray()
            };
        }
        public void Export()
        {
            using (var context = new SmetaContext())
            {
                CodeFormatter fmt = new CodeFormatter();
                var l = P0.Select(x => x.OBOSNOV).
                   Union(Costs.Select(x => x.OBOSNOV)).Distinct().OrderBy(x => x, new AlphaNumericComparer()).ToArray();
                var cds = l.Select(x => new Code() { Name = x, FullName = fmt.ReFormat(x) }).ToArray();
                context.AddRange(cds);
                context.SaveChanges();
                var lbs = LISTBOOKS.Select(listbook => new ListBook
                {
                    TPBASE = listbook.TPBASE,
                    TPBOOK = listbook.TPBOOK,
                    NAME = listbook.NAME,
                    USL = listbook.USL,
                    Books = BOOKS.Where(book => book.CHAPTER != null && book.TPBASE == listbook.TPBASE && book.TPBOOK == listbook.TPBOOK).
                        Select(book => new Book
                        {
                            NAME = book.NAME,
                            OBOSNOV = book.OBOSNOV,
                            DEFINITION = book.DEFINITION,
                            SOKROBOZ = book.SOKROBOZ,
                            COD = book.COD,
                            Chapter = TreeToChapter(book.CHAPTER)
                        }).ToArray()
                }).ToArray();
                context.AddRange(lbs);
                context.SaveChanges();
                var coefs = Listpopravki.Select(x => new Coeficient
                {
                    POPR = x.POPR,
                    NAME = x.NAME,
                    NUMBER = x.NUMBER,
                    NAMEP = x.NAMEP,
                    Labor = x.Labor,
                    Machine = x.Machine,
                    Material = x.Material
                }).ToArray();
                context.AddRange(coefs);
                context.SaveChanges();
                var CodeDic = context.Codes.ToDictionary(x => x.Name, y => y);
                var CoefDic = context.Coeficients.ToDictionary(x => x.POPR, y => y);
                var laws = Costs.Select(x => x.RS.Law.Info).Distinct().Select(x => new Law { Name = x }).ToArray();
                context.AddRange(laws);
                context.SaveChanges();
                //   var codes1 = context.Codes.ToDictionary(x => x.Name, y => y);
                var ls = context.Laws.ToDictionary(x => x.Name, y => y);
                var resourses = Costs.Select(c => new Resourse()
                {
                    Code = CodeDic[c.OBOSNOV],// context.Codes.Where(x => x.Name == c.OBOSNOV).First(),
                   
                    RSIZE = c.RS.Rsize,
                    NAME = c.RS.Name,
                    Info = c.RS.Info,
                    Law = ls[c.RS.Law.Info],
                    Type = c.RS.Type,
                    DoubleCoef = c.RS.DoubleCoef,
                    Works= c.RS.Works,
                    Subworks = c.RS.Subworks
                        .Where(x => CodeDic.ContainsKey(x.OBOSNOV))
                        .Select(x => new EFGetStarted.AspNetCore.NewDb.Models.Subwork()
                        {
                            Code = CodeDic[x.OBOSNOV],
                            Type = x.Type,
                            Value = x.Value
                        }).ToArray()
                }).ToArray();
                context.AddRange(resourses);
                context.SaveChanges();
             //     var CodeDic = context.Codes.ToDictionary(x => x.Name, y => y);
           //     var CoefDic = context.Coeficients.ToDictionary(x => x.POPR, y => y);
                var CodeCoeficients = Poprav.Values.SelectMany(x => x.LPOPR.ToList().Select(y => new CodeCoeficient()
                {
                    Code = CodeDic[x.OBOSNOV],
                    Coeficient = CoefDic[y]
                })).ToArray();
                context.AddRange(CodeCoeficients);
                context.SaveChanges();
                //     var CodeDic = context.Codes.ToDictionary(x => x.Name, y => y);
                var cl = CodeDic.Values.ToList();
                var chapters = context.Chapter.Where(x => x.COD != null && !x.Chapters.Any()).ToList()
                    //.Where                    (c => c.COD.Split('-').Last() == "1")
                    .ToArray();
                ChapterCode[] ChapterCodes =
                    chapters.
                    SelectMany(x => Li(cl,x.COD).Select(y => new ChapterCode { Chapter = x, Code = y })).ToArray();
                context.AddRange(ChapterCodes);
                context.SaveChanges();
            }
        }
        public List<Code> Li(List<Code> dic, string COD)
        {
            if (COD == null)
                return new List<Code>();
            List<string> spl = COD.Split('-').ToList();
            int a;
            if (int.TryParse(spl.Last(), out a))
            {
                spl.RemoveAt(spl.Count - 1);
                COD = string.Join("-", spl);
            }
            var l1 = dic.Where(x => x.Name.StartsWith(COD)).ToList();
            if (l1.Count > 0)
                return l1;
            var l2 = dic.Where(x => x.FullName.StartsWith(COD)).ToList();
            return l2;
        }
        public DbModelTabs()
        {
            BOOKS = Deserialize<BOOK>("BOOK");
            LISTBOOKS = Deserialize<LISTBOOK>("LISTBOOK");
            Listpopravki = Deserialize<Listpopravki>("Listpopravki");
            P0 = Deserialize<P0>("P0201");
            Tasks = P0.Where(x => x.Task != null).ToDictionary(x => x.OBOSNOV, y => y.Task);
            Transports = P0.Where(x => x.Transport != null).ToDictionary(x => x.OBOSNOV, y => y.Transport);
            Materials = P0.Where(x => x.Material != null).ToDictionary(x => x.OBOSNOV, y => y.Material);
            Costs = Deserialize<STable>("S0201");
            var addList = Deserialize<Resurs>("Resurs").Select(x =>
            new STable()
            {
                OBOSNOV = x.OBOSNOV,
                RS = new RS { Name = x.NAME, Rsize = x.RSIZE, Law = new LawContainer(), Works = "", Subworks = new Subwork[0] }
            }
            ).ToList();
            Costs.AddRange(addList);
            CostDic = Costs.Where(y => !y.OBOSNOV.StartsWith("ФС")).ToDictionary(x => x.OBOSNOV, x => x.RS);
            /*var d2 = Deserialize<Resurs>("Resurs").ToDictionary(x => x.OBOSNOV, x => new RS { NAME = x.NAME, RSIZE = x.RSIZE });
            foreach (var k in d2.Keys)
                CostDic[k] = d2[k];*/
                //  Costs = CostDic.Values.ToList();
                Poprav = Deserialize<Poprav>("Poprav").ToDictionary(x => x.OBOSNOV, x => x);
            // Export();
            /* var z = CostDic.Keys.OrderByDescending(x => x.Length).ToArray();
             var alist = CostDic.Keys.Select(x => x.Split('-')[0]).Distinct().ToArray();
             var alist1 = CostDic.Keys.Select(x => ArrIdx( x.Split('-'),1)  ).Distinct().OrderBy(x=>x).ToArray();
             var alist2 = CostDic.Keys.Select(x => ArrIdx(x.Split('-'), 2)).Distinct().OrderBy(x => x).ToArray();
             var alist3 = CostDic.Keys.Select(x => ArrIdx(x.Split('-'), 3)).Distinct().OrderBy(x => x).ToArray();
             var alist4 = CostDic.Keys.Select(x => ArrIdx(x.Split('-'), 4)).Distinct().OrderBy(x => x).ToArray();*/
            /*
            foreach (var v in Books)
                try
            {
                    ExpandTree(v.CHAPTER);
            }
            catch(System.Exception e)
            {
            }
            JsonSerializer serializer = new JsonSerializer();
           File.WriteAllText("c:/clg/books11", JsonConvert.SerializeObject(Books));*/
        }
        public void ExpandTree(TitleTree tree)
        {
            if (tree.CHAPTER == null)// && tree.CHAPTER.First().CHAPTER==null)
            {
                if (!string.IsNullOrWhiteSpace(tree.COD))
                {
                    List<string> spl = tree.COD.Split('-').ToList();
                    if (spl.Count > 1)
                    {
                        spl.RemoveAt(spl.Count - 1);
                        var join = string.Join("-", spl) + '-';
                        var v = CostDic.Keys.Where(x => x.StartsWith(join)).Select(x => new TitleTree()
                        { COD = x, NAME = CostDic[x].Name, CHAPTER = null }).ToList();
                        if (v.Any())
                            tree.CHAPTER = v;
                    }
                }
            }
            else
                for (int i = 0; i < tree.CHAPTER.Count; i++)
                    ExpandTree(tree.CHAPTER[i]);
        }
    }
   
    public class TitleTree
    {
        public TitleTree(XElement xmlTree)
        {
            NAME = xmlTree.Attribute("NR").Value;
            if (xmlTree.Attribute("OB") != null)
                COD = xmlTree.Attribute("OB").Value;
            if (xmlTree.Elements().Any())
            {
                CHAPTER = new List<TitleTree>();
                foreach (var node in xmlTree.Elements())
                {
                    CHAPTER.Add(new TitleTree(node));
                }
            }
        }
        public TitleTree(string xml) : this(XElement.Parse(xml))
        {
        }
        public TitleTree()
        {
        }
        public string NAME { get; set; }
        public string COD { get; set; }
        public List<TitleTree> CHAPTER { get; set; }
    }
   
}
