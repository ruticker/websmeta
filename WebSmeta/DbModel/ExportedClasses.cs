﻿namespace DbModel
{

    public class STable
    {
        public string OBOSNOV { get; set; }
        public RS RS { get; set; }
    }

    public class RS
    {
        public int Sign { get; set; }
        public string Name { get; set; }
        public string Rsize { get; set; }
        public string Info { get; set; }
        public int Type { get; set; }
        public double DoubleCoef { get; set; }
        public string Works { get; set; }
        public LawContainer Law { get; set; }
        public Subwork[] Subworks { get; set; }
    }

    public class LawContainer
    {
        public string Info { get; set; } = "";
    }

    public class Subwork
    {
        public string OBOSNOV { get; set; }
        public int Type { get; set; }
        public float Value { get; set; }
    }




    public class Poprav
    {
        public string OBOSNOV { get; set; }
        public string[] LPOPR { get; set; }
    }
    

    public class LISTBOOK
    {
        public int TPBASE { get; set; }
        public int TPBOOK { get; set; }
        public string NAME { get; set; }
        public string USL { get; set; }
        public object CHAPTER { get; set; }
        public object TCH { get; set; }
    }
    public class BOOK
    {
        public int TPBASE { get; set; }
        public int TPBOOK { get; set; }
        public string OBOSNOV { get; set; }
        public string NAME { get; set; }
        public string COD { get; set; }
        public string SOKROBOZ { get; set; }
        public int DEFINITION { get; set; }
        public TitleTree CHAPTER { get; set; }
    }

   
    public class Resurs
    {
        public string OBOSNOV { get; set; }
        public string NAME { get; set; }
        public string RSIZE { get; set; }
    }
    public class Listpopravki
    {
        public string POPR { get; set; }
        public string NAME { get; set; }
        public string NUMBER { get; set; }
        public string NAMEP { get; set; }
        public float? Labor { get; set; }
        public float? Machine { get; set; }
        public float? Material { get; set; }
    }

    public class P0
    {
        public string OBOSNOV { get; set; }
        public int REGION { get; set; }
        public Material Material { get; set; }
        public Task Task { get; set; }
        public Transport Transport { get; set; }
    }
    public class Material
    {
        public float? Smetnaya { get; set; }
        public float? Otpusk { get; set; }
    }
    public class Task
    {
        public float Total { get; set; }
        public float LaborCost { get; set; }
        public float Machine { get; set; }
        public float Salary { get; set; }
        public float Materials { get; set; }
        public float Labor { get; set; }
        public float Machinist { get; set; }
    }
    public class Transport
    {
        public float Price { get; set; }
    }
}
