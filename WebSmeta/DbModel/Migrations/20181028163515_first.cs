﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DbModel.Migrations
{
    public partial class first : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Chapter",
                columns: table => new
                {
                    ChapterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NAME = table.Column<string>(nullable: true),
                    COD = table.Column<string>(nullable: true),
                    ParentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chapter", x => x.ChapterId);
                    table.ForeignKey(
                        name: "FK_Chapter_Chapter_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Chapter",
                        principalColumn: "ChapterId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Codes",
                columns: table => new
                {
                    CodeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Codes", x => x.CodeId);
                });

            migrationBuilder.CreateTable(
                name: "Coeficients",
                columns: table => new
                {
                    CoeficientId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    POPR = table.Column<string>(nullable: true),
                    NAME = table.Column<string>(nullable: true),
                    NUMBER = table.Column<string>(nullable: true),
                    NAMEP = table.Column<string>(nullable: true),
                    Labor = table.Column<float>(nullable: true),
                    Machine = table.Column<float>(nullable: true),
                    Material = table.Column<float>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coeficients", x => x.CoeficientId);
                });

            migrationBuilder.CreateTable(
                name: "Laws",
                columns: table => new
                {
                    LawId = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Laws", x => x.LawId);
                });

            migrationBuilder.CreateTable(
                name: "ListBooks",
                columns: table => new
                {
                    ListBookId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TPBASE = table.Column<int>(nullable: false),
                    TPBOOK = table.Column<int>(nullable: false),
                    NAME = table.Column<string>(nullable: true),
                    USL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ListBooks", x => x.ListBookId);
                });

            migrationBuilder.CreateTable(
                name: "ChapterCode",
                columns: table => new
                {
                    ChapterId = table.Column<int>(nullable: false),
                    CodeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChapterCode", x => new { x.ChapterId, x.CodeId });
                    table.ForeignKey(
                        name: "FK_ChapterCode_Chapter_ChapterId",
                        column: x => x.ChapterId,
                        principalTable: "Chapter",
                        principalColumn: "ChapterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChapterCode_Codes_CodeId",
                        column: x => x.CodeId,
                        principalTable: "Codes",
                        principalColumn: "CodeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Costs",
                columns: table => new
                {
                    CostId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OBOSNOV = table.Column<string>(nullable: true),
                    REGION = table.Column<int>(nullable: false),
                    Smetnaya = table.Column<float>(nullable: true),
                    Otpusk = table.Column<float>(nullable: true),
                    Total = table.Column<float>(nullable: true),
                    LaborCost = table.Column<float>(nullable: true),
                    Machine = table.Column<float>(nullable: true),
                    Salary = table.Column<float>(nullable: true),
                    Materials = table.Column<float>(nullable: true),
                    Labor = table.Column<float>(nullable: true),
                    Machinist = table.Column<float>(nullable: true),
                    Price = table.Column<float>(nullable: true),
                    CodeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Costs", x => x.CostId);
                    table.ForeignKey(
                        name: "FK_Costs_Codes_CodeId",
                        column: x => x.CodeId,
                        principalTable: "Codes",
                        principalColumn: "CodeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CodeCoeficient",
                columns: table => new
                {
                    CodeId = table.Column<int>(nullable: false),
                    CoeficientId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CodeCoeficient", x => new { x.CodeId, x.CoeficientId });
                    table.ForeignKey(
                        name: "FK_CodeCoeficient_Codes_CodeId",
                        column: x => x.CodeId,
                        principalTable: "Codes",
                        principalColumn: "CodeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CodeCoeficient_Coeficients_CoeficientId",
                        column: x => x.CoeficientId,
                        principalTable: "Coeficients",
                        principalColumn: "CoeficientId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Resourses",
                columns: table => new
                {
                    ResourseId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NAME = table.Column<string>(nullable: true),
                    RSIZE = table.Column<string>(nullable: true),
                    Info = table.Column<string>(nullable: true),
                    CODE2 = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    DoubleCoef = table.Column<double>(nullable: false),
                    Works = table.Column<string>(nullable: true),
                    LawId = table.Column<short>(nullable: false),
                    CodeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resourses", x => x.ResourseId);
                    table.ForeignKey(
                        name: "FK_Resourses_Codes_CodeId",
                        column: x => x.CodeId,
                        principalTable: "Codes",
                        principalColumn: "CodeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Resourses_Laws_LawId",
                        column: x => x.LawId,
                        principalTable: "Laws",
                        principalColumn: "LawId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Books",
                columns: table => new
                {
                    BookId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OBOSNOV = table.Column<string>(nullable: true),
                    NAME = table.Column<string>(nullable: true),
                    COD = table.Column<string>(nullable: true),
                    SOKROBOZ = table.Column<string>(nullable: true),
                    DEFINITION = table.Column<int>(nullable: false),
                    ChapterId = table.Column<int>(nullable: false),
                    ListBookId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Books", x => x.BookId);
                    table.ForeignKey(
                        name: "FK_Books_Chapter_ChapterId",
                        column: x => x.ChapterId,
                        principalTable: "Chapter",
                        principalColumn: "ChapterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Books_ListBooks_ListBookId",
                        column: x => x.ListBookId,
                        principalTable: "ListBooks",
                        principalColumn: "ListBookId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Subworks",
                columns: table => new
                {
                    SubworkId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CodeId = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Value = table.Column<float>(nullable: false),
                    ResourseId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subworks", x => x.SubworkId);
                    table.ForeignKey(
                        name: "FK_Subworks_Codes_CodeId",
                        column: x => x.CodeId,
                        principalTable: "Codes",
                        principalColumn: "CodeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Subworks_Resourses_ResourseId",
                        column: x => x.ResourseId,
                        principalTable: "Resourses",
                        principalColumn: "ResourseId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WorkConsists",
                columns: table => new
                {
                    WorkConsistId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    ResourseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkConsists", x => x.WorkConsistId);
                    table.ForeignKey(
                        name: "FK_WorkConsists_Resourses_ResourseId",
                        column: x => x.ResourseId,
                        principalTable: "Resourses",
                        principalColumn: "ResourseId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Books_ChapterId",
                table: "Books",
                column: "ChapterId");

            migrationBuilder.CreateIndex(
                name: "IX_Books_ListBookId",
                table: "Books",
                column: "ListBookId");

            migrationBuilder.CreateIndex(
                name: "IX_Chapter_ParentId",
                table: "Chapter",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_ChapterCode_CodeId",
                table: "ChapterCode",
                column: "CodeId");

            migrationBuilder.CreateIndex(
                name: "IX_CodeCoeficient_CoeficientId",
                table: "CodeCoeficient",
                column: "CoeficientId");

            migrationBuilder.CreateIndex(
                name: "IX_Costs_CodeId",
                table: "Costs",
                column: "CodeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Resourses_CodeId",
                table: "Resourses",
                column: "CodeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Resourses_LawId",
                table: "Resourses",
                column: "LawId");

            migrationBuilder.CreateIndex(
                name: "IX_Subworks_CodeId",
                table: "Subworks",
                column: "CodeId");

            migrationBuilder.CreateIndex(
                name: "IX_Subworks_ResourseId",
                table: "Subworks",
                column: "ResourseId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkConsists_ResourseId",
                table: "WorkConsists",
                column: "ResourseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Books");

            migrationBuilder.DropTable(
                name: "ChapterCode");

            migrationBuilder.DropTable(
                name: "CodeCoeficient");

            migrationBuilder.DropTable(
                name: "Costs");

            migrationBuilder.DropTable(
                name: "Subworks");

            migrationBuilder.DropTable(
                name: "WorkConsists");

            migrationBuilder.DropTable(
                name: "ListBooks");

            migrationBuilder.DropTable(
                name: "Chapter");

            migrationBuilder.DropTable(
                name: "Coeficients");

            migrationBuilder.DropTable(
                name: "Resourses");

            migrationBuilder.DropTable(
                name: "Codes");

            migrationBuilder.DropTable(
                name: "Laws");
        }
    }
}
