﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
namespace DbModel
{
    public abstract class CodeFormatterAbstract
    {
        protected String Sign;
        protected String Prefix;
        protected int[] numbers;
        protected bool leadingNumber(string s)
        {
            int i = 0;
            return int.TryParse(s[0].ToString(), out i);
        }
        public virtual bool Check(string s)
        {
            return s.StartsWith(Sign);
        }
        public virtual void Decode(string s)
        {
            try
            {
                numbers = (s.Substring(Sign.Length)).Split('-').Select(x => int.Parse(x)).ToArray();
            }
            catch (SystemException e)
            {
                numbers = new int[0];
            }
        }
        public abstract string Format();
        public string TryDecode(string s)
        {
            try
            {
                if (Check(s))
                {
                    Decode(s);
                    return Format();
                }
                else
                    return null;
            }
            catch (SystemException e)
            {
                return null;
            }
        }
    }
    public class CodeFormatterMP : CodeFormatterAbstract
    {
        public CodeFormatterMP()
        {
            Sign = "MP";
            Prefix = "ФЕРмр";
        }
        public override string Format()
        {
            return string.Format("{0}{1}-{2}-{3}-{4}", Prefix,
                numbers[0].ToString().PadLeft(2, '0'),
                numbers[1].ToString().PadLeft(2, '0'),
                numbers[2].ToString().PadLeft(3, '0'),
                numbers[3].ToString().PadLeft(2, '0')
                );
        }
    }
    public class CodeFormatterGP : CodeFormatterAbstract
    {
        public CodeFormatterGP()
        {
            Sign = "ГП";
            Prefix = "ФЕРп";
        }
        public override string Format()
        {
            return string.Format("{0}{1}-{2}-{3}-{4}", Prefix,
                numbers[0].ToString().PadLeft(1, '0'),
                numbers[1].ToString().PadLeft(2, '0'),
                numbers[2].ToString().PadLeft(3, '0'),
                numbers[3].ToString().PadLeft(1, '0')
                );
        }
    }
    public class CodeFormatterGM : CodeFormatterAbstract
    {
        public CodeFormatterGM()
        {
            Sign = "ГМ";
            Prefix = "ФЕРм";
        }
        public override string Format()
        {
            return string.Format("{0}{1}-{2}-{3}-{4}", Prefix,
                 numbers[0].ToString().PadLeft(2, '0'),
                 numbers[1].ToString().PadLeft(2, '0'),
                 numbers[2].ToString().PadLeft(3, '0'),
                 numbers[3].ToString().PadLeft(2, '0')
                 );
        }
    }
    public class CodeFormatterGE3 : CodeFormatterAbstract
    {
        public CodeFormatterGE3()
        {
            Sign = "ГЕ";
            Prefix = "ФЕРр";
        }
        public override bool Check(string s)
        {
            if (base.Check(s))
            {
                Decode(s);
                return numbers.Length == 3;
            }
            return false;
        }
        public override string Format()
        {
            return string.Format("{0}{1}-{2}-{3}", Prefix, numbers[0], numbers[1], numbers[2]);
        }
    }
    public class CodeFormatterGE4 : CodeFormatterAbstract
    {
        public CodeFormatterGE4()
        {
            Sign = "ГЕ";
            Prefix = "ФЕР";
        }
        public override bool Check(string s)
        {
            if (base.Check(s))
            {
                Decode(s);
                return numbers.Length == 4;
            }
            return false;
        }
        public override string Format()
        {
            return string.Format("{0}{1}-{2}-{3}-{4}", Prefix, numbers[0], numbers[1], numbers[2], numbers[3]);
        }
    }
    public class CodeFormatterGE5 : CodeFormatterAbstract
    {
        public CodeFormatterGE5()
        {
            Sign = "";
            Prefix = "ФССЦ";
        }
        public override bool Check(string s)
        {
            if (base.leadingNumber(s))
            {
                Decode(s);
                return numbers.Length == 5;
            }
            return false;
        }
        public override string Format()
        {
            return string.Format("{0}-{1}.{2}.{3}.{4}-{5}", Prefix,
                numbers[0].ToString().PadLeft(2, '0'),
                numbers[1].ToString().PadLeft(1, '0'),
                numbers[2].ToString().PadLeft(2, '0'),
                numbers[3].ToString().PadLeft(2, '0'),
                numbers[4].ToString().PadLeft(4, '0'));
        }
    }
    public class CodeFormatter91x : CodeFormatterAbstract
    {
        public CodeFormatter91x()
        {
            Sign = "";
            Prefix = "ФСЭМ-91.";
        }
        public override bool Check(string s)
        {
            if (base.leadingNumber(s))
            {
                Decode(s);
                return (numbers.Length == 3 && numbers[0] >= 9101);
            }
            return false;
        }
        public override string Format()
        {
            return string.Format("{0}{1}.{2}-{3}", Prefix,
                (numbers[0] % 100).ToString().PadLeft(2, '0'),
                numbers[1].ToString().PadLeft(2, '0'),
                numbers[2].ToString().PadLeft(4, '0'));
        }
    }
    public class CodeFormatterFSS : CodeFormatterAbstract
    {
        public CodeFormatterFSS()
        {
            Sign = "ФССЦПГ";
            Prefix = "ФССЦПг";
        }
        public override string Format()
        {
            if (numbers.Length == 3)
                return string.Format("{0}-{1}-{2}-{3}", Prefix,
               numbers[0].ToString().PadLeft(2, '0'),
               numbers[1].ToString().PadLeft(2, '0'),
               numbers[2].ToString().PadLeft(3, '0')
               );
            else
                return string.Format("{0}-{1}-{2}-{3}-{4}", Prefix,
                    numbers[0].ToString().PadLeft(2, '0'),
                    numbers[1].ToString().PadLeft(2, '0'),
                    numbers[2].ToString().PadLeft(2, '0'),
                    numbers[3].ToString().PadLeft(3, '0')
                    );
        }
    }
    public class CodeFormatter
    {
        List<CodeFormatterAbstract> formats = new List<CodeFormatterAbstract>();
        public CodeFormatter()
        {
            Type ourtype = typeof(CodeFormatterAbstract);
            IEnumerable<Type> list = Assembly.GetAssembly(ourtype).GetTypes().Where(type => type.IsSubclassOf(ourtype));
            foreach (Type type in list)
            {
                ConstructorInfo ctor = type.GetConstructor(new Type[0]);
                formats.Add(ctor.Invoke(new object[] { }) as CodeFormatterAbstract);
            }
        }
        public string ReFormat(string s)
        {
            foreach (var f in formats)
            {
                string s1 = f.TryDecode(s);
                if (s1 != null)
                    return s1;
            }
            return s;
        }
    }
}
