﻿using DbModel;
using EFGetStarted.AspNetCore.NewDb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
namespace WebSmeta.Controllers
{
    public class HomeController : Controller
    {
        //  static DbModelTabs model = new DbModelTabs();
        public HomeController()
        {
            //  var a = model.BOOKS;
        }
        public IActionResult jsonListBooks()
        {
            using (var context = new SmetaContext())
            {
                var r = context.ListBooks.OrderBy(x=>x.TPBOOK)
                    .Select(x => new { Value = x.ListBookId, Text = x.NAME }).ToArray();
                return Json(r);
            }
        }
        class GridLine
        {
            public int Id { get; set; }
            public string COD { get; set; }
            public string NAME { get; set; }
        }
        IEnumerable<GridLine> list(Chapter chapter, SmetaContext context)
        {
            if (chapter.Chapters.Any())
            {
                foreach (var ch in chapter.Chapters)
                    foreach(var v in list(ch, context))
                      yield return v;
            }
              else
            {
                var chapter1 = context.Chapter.Include(x => x.ChapterCodes).ThenInclude(x => x.Code).ThenInclude(x => x.Resourse).
                   SingleOrDefault(x => x.ChapterId == chapter.ChapterId);
                foreach (var v in chapter.ChapterCodes.Select(
                    x => new GridLine
                    {
                        Id = x.Code.CodeId,
                        COD = x.Code.FullName,
                        NAME = x.Code.Resourse.Type +" "+ x.Code.Resourse.NAME
                    }))
                    yield return v;
            }
        }
        public IActionResult jsonCodes(int id)
        {
            using (var context = new SmetaContext())
            {
                var l = context.Chapter
                    .Include(x => x.Chapters)
                    .ThenInclude(x => x.Chapters)
                    .ThenInclude(x => x.Chapters)
                    .ThenInclude(x => x.Chapters)
                    .ThenInclude(x => x.Chapters)
                    .ThenInclude(x => x.Chapters)
                    .SingleOrDefault(x => x.ChapterId == id);
                /*
                var arr = l.COD.Split('-');
                arr = arr.Take(arr.Length - 1).ToArray();
                var s = string.Join('-', arr);
               var aa = context.Codes.Where(x => x.FullName.StartsWith(s)).ToArray();
               */
                var v = list(l, context).ToArray();
                return Json(v.OrderBy(x=>x.COD,new AlphaNumericComparer()));
            }
        }
        public IActionResult jsonchapter(int? ListBookId, string callback, int? id)
        {
            using (var context = new SmetaContext())
            {
                if (!id.HasValue)
                {
                    var res = context.ListBooks.Where(x => x.ListBookId == ListBookId.Value)
                         .Include(x => x.Books)
                         .ThenInclude(x => x.Chapter)
                         .ThenInclude(x => x.Chapters)
                         .SelectMany(x => x.Books.Select(y => y.Chapter))
                         .Select(chapter => new
                         {
                             id = chapter.ChapterId,
                             Name = chapter.NAME,
                             hasChildren = chapter.Chapters.Any(),
                             ReportsTo = chapter.ParentId
                         })
                          .ToArray()
                          .OrderBy(x => x.Name, new AlphaNumericComparer());
                    return Json(res);
                }
                else
                {
                    var res = context.Chapter.Where(chapter => chapter.ParentId == id)
                         .Include(c => c.Chapters)
                         .Select(chapter => new
                         {
                             cod = chapter.COD,
                             id = chapter.ChapterId,
                             Name = chapter.NAME ,
                             hasChildren = chapter.Chapters.Any(),
                             ReportsTo = chapter.ParentId
                         })
                          .ToArray()
                         .OrderBy(x => x.cod, new AlphaNumericComparer())
                        ;
                    return Json(res);
                }
            }
        }
        string FSSC(string s)
        {
            if (s.StartsWith("ФССЦ"))
            {
                string[] l = s.Replace("ФССЦ-", "").Trim().Replace('.', '-').Split('-');
                s = string.Join('-', l.Select(x => int.Parse(x)));
                //    return s;
            }
            return s;
            //  ФССЦ-01.1.01.01-0001
        }
        public IActionResult jsonCosts(int Id)
        {
          //  COD = FSSC(COD);
            using (var context = new SmetaContext())
            {
                var code = context.Codes
                     .Include(c => c.Resourse)
                        .ThenInclude(x => x.Subworks)
                        .ThenInclude(x => x.Code)
                        .ThenInclude(x => x.Resourse)
                     .Include(c => c.Resourse)
                       // .ThenInclude(x => x.Works)
                     .Include(c => c.Resourse)
                        .ThenInclude(x => x.Law)
                     .Include(c => c.CodeCoeficients)
                        .ThenInclude(y => y.Coeficient)
                      .Include(c => c.Cost).SingleOrDefault(x => x.CodeId == Id);
                var res = new
                {
                    Subworks = code.Resourse.Subworks.Select(x => new
                    {
                        OBOSNOV = x.Code.FullName,
                        x.Type,
                        Name = x.Code.Resourse.NAME,
                        Rtype = x.Code.Resourse.Type,
                        x.Code.Resourse.DoubleCoef,
                        x.Value,
                        x.Code.Resourse.RSIZE
                    }).OrderBy(i => i.RSIZE),
                    Law = code.Resourse.Law.Name,
                    Works = NameTos(code.Resourse.Works),//.OrderBy(x => x.Name),//.Select(x => x.Name).ToList(),
                    Popr = code.CodeCoeficients.Select(x => x.Coeficient).ToArray().OrderBy(x => x.POPR, new AlphaNumericComparer()).ToArray(),
                    code.Resourse.RSIZE,
                    Task = 0,
                    Material = 0,
                    Transport = 0
                };
                return Json(res);
            }
        }

        object NameTos(string ss)
        {
            string[] Works;
            try
            {
                Works = JsonConvert.DeserializeObject<string[]>("[" + ss + "]");
            }
            catch
            {
                Works = ss.Split(',');
                for (int i = 0; i < Works.Length; i++)
                    Works[i] = Works[i].Trim().Trim('"');
            }
            return Works.Select(x => new { WorkConsistId = x, Name=x });
        }


        public IActionResult Index()
        {
            ViewData["Title"] = "Смета онлайн";
            return View();
        }
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            return View();
        }
        public IActionResult Error()
        {
            return View();
        }
    }
}
